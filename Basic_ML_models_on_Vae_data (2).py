# HDF5 file handling
import h5py

# Data manipulation and visualization
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Scikit-learn models and utilities
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, roc_curve, auc, precision_recall_curve
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.neighbors import KNeighborsClassifier

# XGBoost and LightGBM models
from xgboost import XGBClassifier
from lightgbm import LGBMClassifier

# TensorFlow and Keras for deep learning
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

# Load the encoded data and labels from the HDF5 file
hdf5_file_path = "encoded_combined_data.h5"

# Open the HDF5 file and read the encoded data and labels into numpy arrays
with h5py.File(hdf5_file_path, "r") as h5f:
    encoded_data = np.array(h5f["encoded_data"])  # Encoded features from the VAE model
    labels = np.array(h5f["labels"])  # Corresponding labels for the encoded data

# Print the shapes of the loaded data for verification
print("Encoded data shape:", encoded_data.shape)
print("Labels shape:", labels.shape)

# Function to plot the confusion matrix
def plot_confusion_matrix(y_true, y_pred, model_name):
    """
    Plots the confusion matrix as a heatmap.

    Args:
        y_true (array): True labels.
        y_pred (array): Predicted labels.
        model_name (str): Name of the model for title display.
    """
    cm = confusion_matrix(y_true, y_pred)  # Generate confusion matrix
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm, annot=True, fmt="d", cmap="Blues")  # Plot the confusion matrix
    plt.title(f'Confusion Matrix for {model_name}')
    plt.xlabel('Predicted')
    plt.ylabel('True')
    plt.show()

# Function to plot the ROC curve and calculate the AUC
def plot_roc_curve(y_true, y_pred_prob, model_name):
    """
    Plots the ROC curve and calculates the Area Under the Curve (AUC).

    Args:
        y_true (array): True labels.
        y_pred_prob (array): Predicted probabilities for the positive class.
        model_name (str): Name of the model for title display.
    """
    fpr, tpr, _ = roc_curve(y_true, y_pred_prob)  # Calculate false positive rate and true positive rate
    roc_auc = auc(fpr, tpr)  # Calculate AUC
    plt.figure(figsize=(8, 6))
    plt.plot(fpr, tpr, color='blue', lw=2, label=f'ROC curve (area = {roc_auc:0.2f})')  # Plot ROC curve
    plt.plot([0, 1], [0, 1], color='red', lw=2, linestyle='--')  # Plot diagonal line
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(f'Receiver Operating Characteristic for {model_name}')
    plt.legend(loc="lower right")
    plt.show()

# Function to plot the Precision-Recall curve
def plot_precision_recall_curve(y_true, y_pred_prob, model_name):
    """
    Plots the Precision-Recall curve.

    Args:
        y_true (array): True labels.
        y_pred_prob (array): Predicted probabilities for the positive class.
        model_name (str): Name of the model for title display.
    """
    precision, recall, _ = precision_recall_curve(y_true, y_pred_prob)  # Calculate precision and recall
    plt.figure(figsize=(8, 6))
    plt.plot(recall, precision, color='blue', lw=2)  # Plot Precision-Recall curve
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title(f'Precision-Recall Curve for {model_name}')
    plt.show()

# Function to train and evaluate multiple machine learning models
def train_and_evaluate_models(X, y):
    """
    Trains and evaluates multiple machine learning models on the given data.

    Args:
        X (array): Encoded input features.
        y (array): Corresponding labels.

    Returns:
        pd.DataFrame: A table with performance metrics for each model.
    """
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    
    # Define a dictionary of machine learning models to train and evaluate
    models = {
        'Logistic Regression': LogisticRegression(max_iter=1000),
        'SVC': SVC(probability=True),
        'Random Forest': RandomForestClassifier(),
        'Naive Bayes': BernoulliNB(),
        'KNN': KNeighborsClassifier(),
        'XGBoost': XGBClassifier(use_label_encoder=False, eval_metric='logloss'),
        'LightGBM': LGBMClassifier()
    }
    
    # Initialize a list to store the results for each model in a table format
    table_data = []
    
    # Iterate over each model for training and evaluation
    for name, model in models.items():
        print(f"Training {name}...")
        model.fit(X_train, y_train)  # Train the model on the training data
        predictions = model.predict(X_test)  # Make predictions on the test data
        predictions_prob = model.predict_proba(X_test)[:, 1] if hasattr(model, "predict_proba") else model.decision_function(X_test)
        accuracy = accuracy_score(y_test, predictions)  # Calculate accuracy
        report = classification_report(y_test, predictions, output_dict=True)  # Generate classification report as a dict
        
        # Extract metrics for the results table
        precision_class_0 = report['0']['precision']
        recall_class_0 = report['0']['recall']
        f1_score_class_0 = report['0']['f1-score']
        precision_class_1 = report['1']['precision']
        recall_class_1 = report['1']['recall']
        f1_score_class_1 = report['1']['f1-score']
        sensitivity = recall_class_1  # Sensitivity is the recall for class 1
        
        # Append the results for this model to the table_data list
        table_data.append({
            'Model': f"{name} (on VAE data)",
            'Accuracy': accuracy,
            'Precision (Class 0)': precision_class_0,
            'Recall (Class 0)': recall_class_0,
            'F1-Score (Class 0)': f1_score_class_0,
            'Precision (Class 1)': precision_class_1,
            'Recall (Class 1)': recall_class_1,
            'F1-Score (Class 1)': f1_score_class_1,
            'Sensitivity (Class 1)': sensitivity
        })
        
        # Plot the confusion matrix, ROC curve, and Precision-Recall curve for the model
        plot_confusion_matrix(y_test, predictions, name)
        plot_roc_curve(y_test, predictions_prob, name)
        plot_precision_recall_curve(y_test, predictions_prob, name)
    
    # Train and evaluate a Neural Network model using TensorFlow/Keras
    print("Training Neural Network...")
    model_nn = Sequential([
        Dense(512, input_shape=(X.shape[1],), activation='relu'),  # Adding hidden layers with ReLU activation
        Dense(256, activation='relu'),
        Dense(128, activation='relu'),
        Dense(64, activation='relu'),
        Dense(1, activation='sigmoid')  # Output layer with sigmoid activation for binary classification
    ])
    
    # Compile the neural network model
    model_nn.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    
    # Train the neural network model
    history = model_nn.fit(X_train, y_train, epochs=5, batch_size=32, verbose=1, validation_split=0.2)
    
    # Evaluate the neural network on the test data
    nn_loss, nn_accuracy = model_nn.evaluate(X_test, y_test, verbose=0)
    predictions_nn_prob = model_nn.predict(X_test).flatten()
    predictions_nn = (predictions_nn_prob > 0.5).astype("int32")
    nn_report = classification_report(y_test, predictions_nn, output_dict=True)
    
    # Extract metrics for the neural network results
    precision_class_0 = nn_report['0']['precision']
    recall_class_0 = nn_report['0']['recall']
    f1_score_class_0 = nn_report['0']['f1-score']
    precision_class_1 = nn_report['1']['precision']
    recall_class_1 = nn_report['1']['recall']
    f1_score_class_1 = nn_report['1']['f1-score']
    sensitivity = recall_class_1  # Sensitivity is the recall for class 1
    
    # Append the neural network results to the table_data list
    table_data.append({
        'Model': 'Neural Network',
        'Accuracy': nn_accuracy,
        'Precision (Class 0)': precision_class_0,
        'Recall (Class 0)': recall_class_0,
        'F1-Score (Class 0)': f1_score_class_0,
        'Precision (Class 1)': precision_class_1,
        'Recall (Class 1)': recall_class_1,
        'F1-Score (Class 1)': f1_score_class_1,
        'Sensitivity (Class 1)': sensitivity
    })
    
    # Plot the confusion matrix, ROC curve, and Precision-Recall curve for the neural network
    plot_confusion_matrix(y_test, predictions_nn, 'Neural Network')
    plot_roc_curve(y_test, predictions_nn_prob, 'Neural Network')
    plot_precision_recall_curve(y_test, predictions_nn_prob, 'Neural Network')
    
    # Convert the table_data list to a DataFrame for displaying the results
    results_table = pd.DataFrame(table_data)
    
    # Sort the results by accuracy in descending order
    results_table = results_table.sort_values(by='Accuracy', ascending=False)
    
    return results_table  # Return the results table

# Call the function to train and evaluate models on the encoded data and labels
results_table = train_and_evaluate_models(encoded_data, labels)

# Display the final results table
results_table

# Save the results table to a CSV file
results_table.to_csv("model_performance_results.csv", index=False)

###########################################################################################################
# Implementing XGBoost (Outside the Function for Focused Evaluation)
###########################################################################################################

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(encoded_data, labels, test_size=0.2, random_state=42)

# Initialize and train the XGBoost model
xgb_model = XGBClassifier(use_label_encoder=False, eval_metric='logloss')
xgb_model.fit(X_train, y_train)

# Make predictions on the test data
y_pred = xgb_model.predict(X_test)
y_pred_prob = xgb_model.predict_proba(X_test)[:, 1]

# Calculate accuracy
accuracy = accuracy_score(y_test, y_pred)
print(f"XGBoost Accuracy: {accuracy:.4f}")

# Generate and print the classification report
report = classification_report(y_test, y_pred)
print("\nClassification Report:\n", report)

# Plot the confusion matrix
cm = confusion_matrix(y_test, y_pred)
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues")
plt.title("Confusion Matrix - XGBoost")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# Plot the ROC curve
fpr, tpr, _ = roc_curve(y_test, y_pred_prob)
roc_auc = auc(fpr, tpr)
plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, color='blue', lw=2, label=f'ROC curve (area = {roc_auc:0.2f})')
plt.plot([0, 1], [0, 1], color='red', lw=2, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve - XGBoost')
plt.legend(loc="lower right")
plt.show()

# Plot the Precision-Recall curve
precision, recall, _ = precision_recall_curve(y_test, y_pred_prob)
plt.figure(figsize=(8, 6))
plt.plot(recall, precision, color='blue', lw=2)
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve - XGBoost')
plt.show()

###########################################################################################################
# Validation of XGBoost Model on Unseen Data
###########################################################################################################

# Load the encoded validation data and labels from the HDF5 file
validation_hdf5_file_path = "encoded_validation_data.h5"

with h5py.File(validation_hdf5_file_path, "r") as h5f:
    encoded_validation_data = np.array(h5f["encoded_data"])
    validation_labels = np.array(h5f["labels"])

# Print the shape of the validation data and labels for verification
print("Encoded validation data shape:", encoded_validation_data.shape)
print("Validation labels shape:", validation_labels.shape)

# Use the trained XGBoost model to make predictions on the validation data
validation_pred = xgb_model.predict(encoded_validation_data)
validation_pred_prob = xgb_model.predict_proba(encoded_validation_data)[:, 1]

# Calculate accuracy on the validation data
validation_accuracy = accuracy_score(validation_labels, validation_pred)
print(f"XGBoost Validation Accuracy: {validation_accuracy:.4f}")

# Generate and print the classification report for the validation data
validation_report = classification_report(validation_labels, validation_pred)
print("\nValidation Classification Report:\n", validation_report)

# Plot the confusion matrix for the validation data
cm = confusion_matrix(validation_labels, validation_pred)
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues")
plt.title("Confusion Matrix - XGBoost (Validation Data)")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# Plot the ROC curve for the validation data
fpr, tpr, _ = roc_curve(validation_labels, validation_pred_prob)
roc_auc = auc(fpr, tpr)
plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, color='blue', lw=2, label=f'ROC curve (area = {roc_auc:0.2f})')
plt.plot([0, 1], [0, 1], color='red', lw=2, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve - XGBoost (Validation Data)')
plt.legend(loc="lower right")
plt.show()

# Plot the Precision-Recall curve for the validation data
precision, recall, _ = precision_recall_curve(validation_labels, validation_pred_prob)
plt.figure(figsize=(8, 6))
plt.plot(recall, precision, color='blue', lw=2)
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve - XGBoost (Validation Data)')
plt.show()