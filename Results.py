# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 16:49:35 2024

@author: nu839618
"""

import h5py
import numpy as np
import pandas as pd
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, roc_curve, auc, precision_recall_curve
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import RandomizedSearchCV

# Function to handle data processing for both UNet and CWGAN-GP
def process_synthetic_data(syn_data, encoded_data, labels, data_type):
    # Reduce dimensions to 2D using PCA for visualization
    pca = PCA(n_components=2)
    reduced_data = pca.fit_transform(syn_data)

    # Apply KMeans clustering to the reduced data
    n_clusters = 2  
    kmeans = KMeans(n_clusters=n_clusters, random_state=42)
    kmeans.fit(reduced_data)

    # Get the cluster labels assigned by KMeans
    cluster_labels = kmeans.labels_

    # Correcting the class label assignment based on centroids
    class_0_label = np.argmin(kmeans.cluster_centers_[:, 0])  # Label spread cluster as class 0
    class_1_label = np.argmax(kmeans.cluster_centers_[:, 0])  # Label compact cluster as class 1

    # Map cluster labels to actual class labels
    pseudo_labels = np.where(cluster_labels == class_0_label, 0, 1)

    # Plot the KMeans clusters with assigned pseudo-labels
    plt.figure(figsize=(10, 8))
    scatter = plt.scatter(reduced_data[:, 0], reduced_data[:, 1], c=pseudo_labels, cmap='viridis', s=50, alpha=0.7)
    plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=300, c='red', label='Centroids')
    plt.title(f'KMeans Clustering of {data_type} Synthetic Data with Pseudo Labels')
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')

    # Add class labels to the legend
    handles, _ = scatter.legend_elements(prop="colors")
    legend_labels = ["Class 0", "Class 1"]
    plt.legend(handles, legend_labels + ['Centroids'], title="Classes")

    plt.show()

    # Revert PCA-transformed points back to the original space
    reverted_data = pca.inverse_transform(reduced_data)

    # Merge pseudo-labeled synthetic data with real encoded data
    merged_data = np.concatenate((encoded_data, reverted_data), axis=0)
    merged_labels = np.concatenate((labels, pseudo_labels), axis=0)

    return merged_data, merged_labels

# Load real data
with h5py.File("encoded_combined_data.h5", "r") as h5f:
    encoded_data = np.array(h5f["encoded_data"])
    labels = np.array(h5f["labels"])

# Process UNet synthetic data
with h5py.File("unet_syn_data.h5", "r") as h5f:
    unet_syn_data = np.array(h5f["unet_syn_data"])
unet_merged_data, unet_merged_labels = process_synthetic_data(unet_syn_data, encoded_data, labels, "unet")

# Process CWGAN-GP synthetic data
with h5py.File("cwgangp_syn_data.h5", "r") as h5f:
    cwgan_syn_data = np.array(h5f["cwgangp_syn_data"])
cwgan_merged_data, cwgan_merged_labels = process_synthetic_data(cwgan_syn_data, encoded_data, labels, "cwgan")

# Train and evaluate XGBoost on different data combinations with cross-validation
def evaluate_xgb_model_cv(X, y, model_name, n_splits=5):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    
    accuracy_scores = []
    metrics_per_class = {"Class 0": {"precision": [], "recall": [], "f1-score": []},
                         "Class 1": {"precision": [], "recall": [], "f1-score": []}}

    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        clf = XGBClassifier(n_estimators=100, random_state=42, use_label_encoder=False, eval_metric='logloss')
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)

        accuracy = accuracy_score(y_test, y_pred)
        accuracy_scores.append(accuracy)

        report = classification_report(y_test, y_pred, output_dict=True)

        for cls in ["0", "1"]:
            class_label = f"Class {cls}"
            metrics_per_class[class_label]["precision"].append(report[cls]["precision"])
            metrics_per_class[class_label]["recall"].append(report[cls]["recall"])
            metrics_per_class[class_label]["f1-score"].append(report[cls]["f1-score"])

    avg_accuracy = np.mean(accuracy_scores)
    avg_metrics = {}
    for cls in metrics_per_class:
        avg_metrics[cls] = {metric: np.mean(values) for metric, values in metrics_per_class[cls].items()}

    # No rounding off of values, maintaining the full precision
    return [
        model_name, 
        avg_accuracy, 
        avg_metrics["Class 0"]["precision"], avg_metrics["Class 0"]["recall"], avg_metrics["Class 0"]["f1-score"],
        avg_metrics["Class 1"]["precision"], avg_metrics["Class 1"]["recall"], avg_metrics["Class 1"]["f1-score"],
        avg_metrics["Class 1"]["recall"]  # Sensitivity is the same as recall for Class 1
    ]

# Store evaluation results in a list
results = []

# Evaluate original XGBoost model with cross-validation
results.append(evaluate_xgb_model_cv(encoded_data, labels, "XGBoost (original)", n_splits=5))

# Evaluate XGBoost model with UNet synthetic data
results.append(evaluate_xgb_model_cv(unet_merged_data, unet_merged_labels, "XGBoost (original+unet)", n_splits=5))

# Evaluate XGBoost model with CWGAN-GP synthetic data
results.append(evaluate_xgb_model_cv(cwgan_merged_data, cwgan_merged_labels, "XGBoost (original+cwgan-gp)", n_splits=5))

# Combine UNet and CWGAN-GP synthetic data
unet_cwgan_combined_data = np.concatenate([unet_merged_data, cwgan_merged_data], axis=0)
unet_cwgan_combined_labels = np.concatenate([unet_merged_labels, cwgan_merged_labels], axis=0)

# Evaluate XGBoost model with combined UNet and CWGAN-GP synthetic data
results.append(evaluate_xgb_model_cv(unet_cwgan_combined_data, unet_cwgan_combined_labels, "XGBoost (original+unet+cwgan-gp)", n_splits=5))

# Convert results to a readable table format
results_table1 = [["Model", "Accuracy", "Precision (Class 0)", "Recall (Class 0)", "F1-Score (Class 0)", 
                  "Precision (Class 1)", "Recall (Class 1)", "F1-Score (Class 1)", "Sensitivity (Class 1)"]]

for result in results:
    results_table1.append(result)

# Display the results table
for row in results_table1:
    print("\t".join(map(str, row)))

# Convert results table to DataFrame with the model name included
df_results1 = pd.DataFrame(results_table1)

# Save the sorted results to a CSV file
df_results1.to_csv("xgboost_results1.csv", index=False)

print(df_results1)

# Function to combine datasets in different proportions
def combine_datasets(original_data, original_labels, syn1_data, syn1_labels, syn2_data, syn2_labels, proportion1, proportion2):
    syn1_size = int(len(syn1_data) * proportion1)
    syn2_size = int(len(syn2_data) * proportion2)
    
    combined_data = np.concatenate([original_data, syn1_data[:syn1_size], syn2_data[:syn2_size]], axis=0)
    combined_labels = np.concatenate([original_labels, syn1_labels[:syn1_size], syn2_labels[:syn2_size]], axis=0)
    
    return combined_data, combined_labels

# Function to perform cross-validation and evaluate XGBoost using median
def cross_val_xgb(X, y, n_splits=5):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    accuracy_scores = []
    metrics_per_class = {"Class 0": {"precision": [], "recall": [], "f1-score": [], "sensitivity": []},
                         "Class 1": {"precision": [], "recall": [], "f1-score": [], "sensitivity": []}}

    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        
        clf = XGBClassifier(n_estimators=100, random_state=42, use_label_encoder=False, eval_metric='logloss')
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        
        accuracy = accuracy_score(y_test, y_pred)
        accuracy_scores.append(accuracy)

        report = classification_report(y_test, y_pred, output_dict=True)

        for cls in ["0", "1"]:
            class_label = f"Class {cls}"
            metrics_per_class[class_label]["precision"].append(report[cls]["precision"])
            metrics_per_class[class_label]["recall"].append(report[cls]["recall"])
            metrics_per_class[class_label]["f1-score"].append(report[cls]["f1-score"])
            metrics_per_class[class_label]["sensitivity"].append(report[cls]["recall"])  # Sensitivity is the same as recall

    # Use median instead of mean
    median_accuracy = np.median(accuracy_scores)
    median_metrics = {}
    for cls in metrics_per_class:
        median_metrics[cls] = {metric: np.median(values) for metric, values in metrics_per_class[cls].items()}

    return median_accuracy, median_metrics

# Evaluate different proportions with cross-validation
results_table = []

for proportion1 in np.arange(0.1, 1.1, 0.1):  # Loop over proportions for UNet
    for proportion2 in np.arange(0.1, 1.1, 0.1):  # Loop over proportions for CWGAN-GP
        combined_data, combined_labels = combine_datasets(encoded_data, labels, unet_merged_data, unet_merged_labels, cwgan_merged_data, cwgan_merged_labels, proportion1, proportion2)
        
        # Perform cross-validation with median
        median_accuracy, median_metrics = cross_val_xgb(combined_data, combined_labels, n_splits=5)
        
        # Store results
        results_table.append([
            f"XGBoost (CV=5)",
            proportion1 * 100, proportion2 * 100, median_accuracy,
            median_metrics["Class 0"]["precision"], median_metrics["Class 0"]["recall"], median_metrics["Class 0"]["f1-score"],
            median_metrics["Class 1"]["precision"], median_metrics["Class 1"]["recall"], median_metrics["Class 1"]["f1-score"],
            median_metrics["Class 1"]["sensitivity"]
        ])

# Sort the results by accuracy in descending order
results_table_sorted = sorted(results_table, key=lambda x: x[3], reverse=True)

# Display the sorted results in a table format
print("Model\tUnet (%)\tCwgan-gp (%)\tAccuracy\tPrecision (Class 0)\tRecall (Class 0)\tF1-Score (Class 0)\tPrecision (Class 1)\tRecall (Class 1)\tF1-Score (Class 1)\tSensitivity (Class 1)")
for row in results_table_sorted:
    print(f"{row[0]}\t{row[1]:.1f}\t{row[2]:.1f}\t{row[3]:.6f}\t{row[4]:.6f}\t{row[5]:.6f}\t{row[6]:.6f}\t{row[7]:.6f}\t{row[8]:.6f}\t{row[9]:.6f}\t{row[10]:.6f}")

    
# After finding the best proportions, train on the full combined dataset and evaluate
best_proportion1, best_proportion2, best_accuracy = results_table_sorted[0][1], results_table_sorted[0][2], results_table_sorted[0][3]
final_data, final_labels = combine_datasets(encoded_data, labels, unet_merged_data, unet_merged_labels, cwgan_merged_data, cwgan_merged_labels, best_proportion1 / 100, best_proportion2 / 100)

# Split the final data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(final_data, final_labels, test_size=0.3, random_state=42, stratify=final_labels)

# Initialize the XGBoost classifier
final_Xgb = XGBClassifier(n_estimators=100, random_state=42, use_label_encoder=False, eval_metric='logloss')

# Train the model on the training set
final_Xgb.fit(X_train, y_train)

# Predict on the test set
y_pred = final_Xgb.predict(X_test)

# Final metrics
accuracy = accuracy_score(y_test, y_pred)
report = classification_report(y_test, y_pred, output_dict=True)

print(f"Final Accuracy: {accuracy}")
print("Final Classification Report:\n", classification_report(y_test, y_pred))

# Confusion matrix
cm = confusion_matrix(y_test, y_pred)
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
plt.title("Confusion Matrix")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# Convert results table to DataFrame with the model name included
df_results = pd.DataFrame(results_table, columns=[
    "Model", "UNet (%)", "CWGAN-GP (%)", "Accuracy", "Precision (Class 0)", 
    "Recall (Class 0)", "F1-Score (Class 0)", "Precision (Class 1)", 
    "Recall (Class 1)", "F1-Score (Class 1)", "Sensitivity (Class 1)"
])

# Sort the results by accuracy in descending order
df_results_sorted = df_results.sort_values(by="Accuracy", ascending=False)

# Save the sorted results to a CSV file
df_results_sorted.to_csv("xgboost_cross_val_results2.csv", index=False)

# Display the sorted results in a table format
print(df_results_sorted)

# ================================
# Load validation data
# ================================
with h5py.File("encoded_validation_data.h5", "r") as h5f:
    X_val = np.array(h5f["encoded_data"])
    y_val = np.array(h5f["labels"])

# ================================
# 1. Create the base model using only the original data
# ================================
X_train_base, X_test_base, y_train_base, y_test_base = train_test_split(encoded_data, labels, test_size=0.3, random_state=42, stratify=labels)

base_Xgb = XGBClassifier(n_estimators=100, random_state=42, use_label_encoder=False, eval_metric='logloss')
base_Xgb.fit(X_train_base, y_train_base)

# Evaluate on validation set (unseen data)
y_pred_base_val = base_Xgb.predict(X_val)
y_pred_proba_base_val = base_Xgb.predict_proba(X_val)[:, 1]

# Confusion matrix for validation
cm_base_val = confusion_matrix(y_val, y_pred_base_val)
sns.heatmap(cm_base_val, annot=True, fmt='d', cmap='Blues')
plt.title("Confusion Matrix - Base Model (Validation Set)")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# ROC curve and Precision-Recall curve for validation
def plot_curves(y_true, y_pred_proba, model_name):
    fpr, tpr, _ = roc_curve(y_true, y_pred_proba)
    precision, recall, _ = precision_recall_curve(y_true, y_pred_proba)
    roc_auc = auc(fpr, tpr)

    plt.figure(figsize=(12, 6))

    # ROC curve
    plt.subplot(1, 2, 1)
    plt.plot(fpr, tpr, label=f'ROC curve (AUC = {roc_auc:.2f})')
    plt.plot([0, 1], [0, 1], 'k--')
    plt.title(f'ROC Curve - {model_name}')

    # Precision-Recall curve
    plt.subplot(1, 2, 2)
    plt.plot(recall, precision, label='Precision-Recall curve')
    plt.title(f'Precision-Recall Curve - {model_name}')

    plt.tight_layout()
    plt.show()

# Plot ROC and Precision-Recall curves for base model
plot_curves(y_val, y_pred_proba_base_val, 'Base Model (Validation Set)')

# ================================
# 2. Create the best model based on best proportions
# ================================
# Use the best proportions found during cross-validation
final_data_best, final_labels_best = combine_datasets(encoded_data, labels, unet_merged_data, unet_merged_labels, cwgan_merged_data, cwgan_merged_labels, best_proportion1 / 100, best_proportion2 / 100)

# Split final data into train and test sets
X_train_best, X_test_best, y_train_best, y_test_best = train_test_split(final_data_best, final_labels_best, test_size=0.3, random_state=42, stratify=final_labels_best)

best_Xgb = XGBClassifier(n_estimators=100, random_state=42, use_label_encoder=False, eval_metric='logloss')
best_Xgb.fit(X_train_best, y_train_best)

# Evaluate on validation set (unseen data)
y_pred_best_val = best_Xgb.predict(X_val)
y_pred_proba_best_val = best_Xgb.predict_proba(X_val)[:, 1]

# Confusion matrix for best model
cm_best_val = confusion_matrix(y_val, y_pred_best_val)
sns.heatmap(cm_best_val, annot=True, fmt='d', cmap='Blues')
plt.title("Confusion Matrix - Best Model (Validation Set)")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# Plot ROC and Precision-Recall curves for best model
plot_curves(y_val, y_pred_proba_best_val, 'Best Model (Validation Set)')
print(classification_report(y_val, y_pred_best_val))
      
# ================================
# 3. Hyperparameter Tuning (HPT) and Cross-Validation (CV) for the best model
# ================================

param_grid = {
    'learning_rate': [0.01, 0.1, 0.2, 0.3],
    'n_estimators': [50, 100, 200, 500],
    'max_depth': [3, 5, 7, 9],
    'min_child_weight': [1, 3, 5],
    'subsample': [0.6, 0.8, 1.0],
    'colsample_bytree': [0.6, 0.8, 1.0],
    'gamma': [0, 0.1, 0.5, 1],
}

random_search = RandomizedSearchCV(estimator=XGBClassifier(random_state=42, use_label_encoder=False, eval_metric='logloss'),
                                   param_distributions=param_grid,
                                   n_iter=50, scoring='accuracy', n_jobs=-1, cv=5, verbose=1, random_state=42)

# Perform hyperparameter tuning on the training set
random_search.fit(X_train_best, y_train_best)

# Best parameters from RandomizedSearchCV
best_params = random_search.best_params_
print("Best parameters found: ", best_params)

# Train the optimized model using best parameters
optimized_Xgb = XGBClassifier(**best_params, random_state=42, use_label_encoder=False, eval_metric='logloss')
optimized_Xgb.fit(X_train_best, y_train_best)

# ================================
# Box Plot for Cross-Validation Results with 5 metrics
# ================================
def cross_val_box_plot(random_search):
    # Extract cross-validation results
    cv_results = random_search.cv_results_

    # Create a DataFrame with the metrics
    df_cv_results = pd.DataFrame({
        'Accuracy': cv_results['mean_test_score'],
        'Precision': cv_results['mean_test_score'],  
        'Recall': cv_results['mean_test_score'],     
        'F1-Score': cv_results['mean_test_score'],   
        'Sensitivity': cv_results['mean_test_score'] 
    })

    # Box plot
    df_cv_results.boxplot(column=['Accuracy', 'Precision', 'Recall', 'F1-Score', 'Sensitivity'])
    plt.title('Cross-Validation Box Plot for Metrics')
    plt.ylabel('Score')
    plt.show()

# Plot box plot for cross-validation
cross_val_box_plot(random_search)

# Save the optimized model
import joblib
joblib.dump(optimized_Xgb, 'final_xgboost_model.pkl')

# ================================
# 4. Evaluate the final model on validation data
# ================================
y_pred_final_val = optimized_Xgb.predict(X_val)
y_pred_proba_final_val = optimized_Xgb.predict_proba(X_val)[:, 1]

# Confusion matrix for final model
cm_final_val = confusion_matrix(y_val, y_pred_final_val)
sns.heatmap(cm_final_val, annot=True, fmt='d', cmap='Blues')
plt.title("Confusion Matrix - Final Model (Validation Set)")
plt.xlabel("Predicted")
plt.ylabel("True")
plt.show()

# Plot ROC and Precision-Recall curves for final model
plot_curves(y_val, y_pred_proba_final_val, 'Final Model (Validation Set)')
print(classification_report(y_val,y_pred_final_val))

########################## END #####################################################################################