# Standard Python Libraries
import json

# Third-Party Libraries
import numpy as np
import matplotlib.pyplot as plt
import h5py

# TensorFlow and Keras Libraries
import tensorflow as tf
from tensorflow.keras import layers, models, optimizers, callbacks

# Scikit-learn Libraries
from sklearn.model_selection import StratifiedShuffleSplit, train_test_split
from sklearn.decomposition import PCA
from sklearn.metrics import mean_squared_error, r2_score, explained_variance_score
from sklearn.metrics.pairwise import cosine_similarity

# Optuna for Hyperparameter Optimization
import optuna

# NLTK for Natural Language Processing
from nltk.translate.bleu_score import sentence_bleu

# Levenshtein Distance for String Matching
from Levenshtein import distance as levenshtein_distance

# Tqdm for Progress Bars
from tqdm.keras import TqdmCallback


# Setting GPU memory growth
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

# Define the U-Net model with 1D convolutions using TensorFlow/Keras
def unet_1d_model(input_shape, base_filters, depth, dropout_rate):
    inputs = layers.Input(shape=input_shape)
    
    # Encoder
    x = inputs
    skips = []
    for i in range(depth):
        x = contract_block(x, base_filters * (2 ** i), dropout_rate)
        skips.append(x)
    
    # Bottleneck
    x = contract_block(x, base_filters * (2 ** depth), dropout_rate)
    
    # Decoder
    for i in range(depth - 1, -1, -1):
        x = upconv_block(x, skips[i], base_filters * (2 ** i), dropout_rate)
    
    # Additional upsampling layer to restore the original input shape
    x = layers.Conv1DTranspose(1, kernel_size=2, strides=2, padding='same')(x)
    
    # Output layer
    outputs = layers.Conv1D(1, kernel_size=1, padding='same', activation='linear')(x)
    
    # Model
    model = models.Model(inputs, outputs)
    return model

def contract_block(x, filters, dropout_rate):
    x = layers.Conv1D(filters, kernel_size=3, padding='same', activation='relu')(x)
    x = layers.BatchNormalization()(x)
    x = layers.Conv1D(filters, kernel_size=3, padding='same', activation='relu')(x)
    x = layers.BatchNormalization()(x)
    x = layers.MaxPooling1D(pool_size=2)(x)
    x = layers.Dropout(dropout_rate)(x)
    return x

def upconv_block(x, skip, filters, dropout_rate):
    x = layers.Conv1DTranspose(filters, kernel_size=2, strides=2, padding='same')(x)
    
    # Adjust skip connection to match dimensions
    if x.shape[1] != skip.shape[1]:
        diff = skip.shape[1] - x.shape[1]
        if diff > 0:
            skip = layers.Cropping1D(cropping=(0, diff))(skip)
        else:
            x = layers.Cropping1D(cropping=(0, -diff))(x)
    
    x = layers.Concatenate()([x, skip])
    x = layers.Conv1D(filters, kernel_size=3, padding='same', activation='relu')(x)
    x = layers.BatchNormalization()(x)
    x = layers.Conv1D(filters, kernel_size=3, padding='same', activation='relu')(x)
    x = layers.BatchNormalization()(x)
    x = layers.Dropout(dropout_rate)(x)
    return x

# Load and prepare data
hdf5_file_path = "encoded_combined_data.h5"
with h5py.File(hdf5_file_path, "r") as h5f:
    encoded_data = np.array(h5f["encoded_data"])
    labels = np.array(h5f["labels"])

# Convert data to TensorFlow tensors and reshape for 1D convolutions
encoded_data = np.expand_dims(encoded_data, axis=-1)  # Add channel dimension

# Split the data into training and validation sets for hyperparameter tuning (20% of the data)
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
labels_for_split = np.zeros(len(encoded_data))  # Dummy labels for StratifiedShuffleSplit
train_idx, val_idx = next(sss.split(encoded_data, labels_for_split))

X_train_hpt, X_val_hpt = encoded_data[train_idx], encoded_data[val_idx]

# Define early stopping and learning rate scheduler
early_stopping = callbacks.EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)

# Dynamic learning rate with ExponentialDecay
def train_and_evaluate(base_filters, learning_rate, epochs, dropout_rate, depth):
    input_shape = X_train_hpt.shape[1:]
    model = unet_1d_model(input_shape, base_filters, depth, dropout_rate)
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate=learning_rate,
        decay_steps=5000,
        decay_rate=0.9
    )
    model.compile(optimizer=optimizers.Adam(learning_rate=lr_schedule),
                  loss='mean_squared_error')  # Reconstruction loss
    
    history = model.fit(X_train_hpt, X_train_hpt,  # Using input as target for reconstruction
                        validation_data=(X_val_hpt, X_val_hpt),  # Same for validation
                        batch_size=64,
                        epochs=epochs,
                        callbacks=[early_stopping, TqdmCallback(verbose=1)],
                        verbose=0)
    
    val_loss = np.min(history.history['val_loss'])  # Best validation loss
    return val_loss

def objective(trial):
    base_filters = trial.suggest_int('base_filters', 32, 128, step=32)
    learning_rate = trial.suggest_float('learning_rate', 1e-5, 1e-2, log=True)
    epochs = trial.suggest_int('epochs', 10, 50)
    dropout_rate = trial.suggest_float('dropout_rate', 0.1, 0.5)
    depth = trial.suggest_int('depth', 3, 5)  # Depth of the network

    val_loss = train_and_evaluate(base_filters, learning_rate, epochs, dropout_rate, depth)
    return val_loss

# Run Optuna for hyperparameter tuning on 20% of the data
study = optuna.create_study(direction='minimize')
study.optimize(objective, n_trials=10)
best_params = study.best_params
print('Best Hyperparameters:', best_params)

# Split data again with different 20% for validation using the remaining 80% of the data
X_train_final, X_val_final = train_test_split(
    encoded_data, test_size=0.2, random_state=101, stratify=labels_for_split)

# Train the final model with the entire dataset using the best hyperparameters
def train_final_model(base_filters, learning_rate, epochs, dropout_rate, depth):
    input_shape = X_train_final.shape[1:]
    model = unet_1d_model(input_shape, base_filters, depth, dropout_rate)
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate=learning_rate,
        decay_steps=10000,
        decay_rate=0.9
    )
    model.compile(optimizer=optimizers.Adam(learning_rate=lr_schedule),
                  loss='mean_squared_error')  # Reconstruction loss

    # Define checkpoints and logging
    csv_logger = callbacks.CSVLogger('training_log.csv')

    history = model.fit(X_train_final, X_train_final,  # Training with reconstruction loss
                        validation_data=(X_val_final, X_val_final),  # Validation with reconstruction loss
                        batch_size=64,
                        epochs=epochs,
                        callbacks=[early_stopping, csv_logger, TqdmCallback(verbose=1)],
                        verbose=1)
    
    return history, model

# Train and log results with best hyperparameters on the entire dataset
history, final_model = train_final_model(best_params['base_filters'], best_params['learning_rate'], best_params['epochs'], best_params['dropout_rate'], best_params['depth'])

# Plot the training and validation losses
plt.plot(history.history['loss'], label='Training Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.title('Training and Validation Losses of UNet Model')
plt.show()


# Generate synthetic data using the trained model

def generate_synthetic_data(model, input_data, num_samples):
    synthetic_data = model.predict(input_data[:num_samples])
    return synthetic_data

num_synthetic_samples = 10000  # Number of synthetic samples

synthetic_data = generate_synthetic_data(final_model, X_train_final, num_synthetic_samples)

# Evaluating synthetic data generated

def evaluate_synthetic_data(synthetic_data, real_data):
    """
    Evaluates the generated synthetic data against the real data.

    Parameters:
    - synthetic_data: The generated synthetic data.
    - real_data: The real data to compare against.

    Prints:
    - Reconstruction metrics: MSE, R2 Score, Explained Variance Score, BLEU score, Cosine Similarity, Levenshtein Distance.
    - Distributional Comparison: Mean Difference, Variance Difference.
    - PCA Visualization of real vs. synthetic data.
    """
    assert synthetic_data.shape == real_data.shape, f"Shape mismatch: Synthetic {synthetic_data.shape}, Real {real_data.shape}"
    
    synthetic_data_flat = synthetic_data.reshape(synthetic_data.shape[0], -1)
    real_data_flat = real_data.reshape(real_data.shape[0], -1)
    
    # Calculate reconstruction metrics
    mse = mean_squared_error(real_data_flat, synthetic_data_flat)
    r2 = r2_score(real_data_flat, synthetic_data_flat)
    evs = explained_variance_score(real_data_flat, synthetic_data_flat)
    
    print(f'MSE: {mse:.4f}')
    print(f'R2 Score: {r2:.4f}')
    print(f'Explained Variance Score: {evs:.4f}')

    # Calculate BLEU score
    bleu_scores = []
    for i in range(real_data_flat.shape[0]):
        real_seq = [int(x) for x in real_data_flat[i]]
        synthetic_seq = [int(x) for x in synthetic_data_flat[i]]
        bleu_score = sentence_bleu([real_seq], synthetic_seq)
        bleu_scores.append(bleu_score)
    
    avg_bleu_score = np.mean(bleu_scores)
    print(f'Average BLEU Score: {avg_bleu_score:.4f}')
    
    # Calculate Cosine Similarity
    avg_cosine_similarity = np.mean([cosine_similarity([real_data_flat[i]], [synthetic_data_flat[i]])[0, 0] for i in range(real_data_flat.shape[0])])
    print(f'Average Cosine Similarity: {avg_cosine_similarity:.4f}')
    
    # Calculate Levenshtein Distance
    levenshtein_distances = []
    for i in range(real_data_flat.shape[0]):
        real_seq = real_data_flat[i].astype(int)
        synthetic_seq = synthetic_data_flat[i].astype(int)
        levenshtein_distances.append(levenshtein_distance(real_seq, synthetic_seq))
    
    avg_levenshtein_distance = np.mean(levenshtein_distances)
    print(f'Average Levenshtein Distance: {avg_levenshtein_distance:.4f}')
    
    # Distributional Comparison
    mean_diff = np.abs(np.mean(real_data_flat) - np.mean(synthetic_data_flat))
    var_diff = np.abs(np.var(real_data_flat) - np.var(synthetic_data_flat))
    
    print(f'Mean Difference: {mean_diff:.4f}')
    print(f'Variance Difference: {var_diff:.4f}')
    
    # PCA Visualization for synthetic and real data
    pca = PCA(n_components=2)
    real_data_pca = pca.fit_transform(real_data_flat)
    synthetic_data_pca = pca.transform(synthetic_data_flat)
    
    plt.figure(figsize=(10, 6))
    plt.scatter(real_data_pca[:, 0], real_data_pca[:, 1], color='blue', label='Real Data', alpha=0.5)
    plt.scatter(synthetic_data_pca[:, 0], synthetic_data_pca[:, 1], color='red', label='Synthetic Data', alpha=0.5)
    plt.title('PCA of Real vs Synthetic Data')
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')
    plt.legend()
    plt.show()

# Evaluate the synthetic data against real data
evaluate_synthetic_data(synthetic_data, X_train_final[:num_synthetic_samples])

#  synthetic_data with shape (10000, 448, 1)
synthetic_data = synthetic_data.squeeze()  # Remove the extra dimension at the end

# Check the new shape
synthetic_data.shape

print('Best Hyperparameters:', best_params)

# Save best parameters to a JSON file
best_params_file = 'best_hyperparameters.json'

with open(best_params_file, 'w') as f:
    json.dump(best_params, f)

print(f'Best hyperparameters saved to {best_params_file}')

# Save the data as h5py format
output_file_path = "unet_syn_data.h5"
with h5py.File(output_file_path, "w") as h5f:
    h5f.create_dataset("unet_syn_data", data=synthetic_data)
    

