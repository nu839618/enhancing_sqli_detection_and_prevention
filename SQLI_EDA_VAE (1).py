# Data Manipulation and Analysis
import pandas as pd
import numpy as np

# Data Visualization
import matplotlib.pyplot as plt
import seaborn as sns
from wordcloud import WordCloud

# Natural Language Processing and Embeddings
from gensim.models.fasttext import FastText
import gensim.downloader as api
from gensim.models import KeyedVectors

# Machine Learning and Model Evaluation
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import classification_report, accuracy_score, precision_recall_curve, auc, average_precision_score, mean_absolute_error, mean_squared_error, confusion_matrix, ConfusionMatrixDisplay, silhouette_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier

# Deep Learning
import tensorflow as tf
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Dropout, Input, LeakyReLU, BatchNormalization, LSTM
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import layers, regularizers
import tensorflow.keras.backend as K
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN
from tensorflow.keras.losses import MeanSquaredError

# Transformers and Tokenizers
from transformers import DistilBertTokenizer, TFDistilBertModel
from tokenizers import ByteLevelBPETokenizer
from tensorflow.keras.preprocessing.text import Tokenizer

# Additional Tools
import re
import requests
import gzip
import joblib
import h5py
import random
import time
from collections import Counter

# Hyperparameter Tuning
from keras_tuner import HyperModel
from keras_tuner.tuners import BayesianOptimization

# Load the datasets
df1 = pd.read_csv('sqli.csv', encoding='utf-16', delimiter='\t')
df2 = pd.read_csv("Modified_SQL_Dataset.csv")

# Preprocess the first dataset
df1['Label'] = df1.iloc[:, 0].astype(str).str[-1]
df1['Query'] = df1.iloc[:, 0].astype(str).str[:-2]
df1 = df1[['Query', 'Label']]
df1['Label'] = df1['Label'].astype(int)

# Merge the datasets into one
df = pd.concat([df1, df2], ignore_index=True)
df.drop_duplicates(inplace=True)
df = df[df['Query'].str.len() >= 3]

# Split the data into main and holdout sets
main_df, df_holdout = train_test_split(df, test_size=0.01, random_state=463)

# Check the distribution of labels in the merged dataset
label_distribution = df['Label'].value_counts()
print(label_distribution)

# Defining a set of SQL keywords and symbols for detecting SQL syntax in text
sql_keywords = {
    'select', 'from', 'where', 'insert', 'update', 'delete', 'union', 'join', 'create', 'drop',
    'alter', 'truncate', 'replace', 'distinct', 'into', 'values', 'set', 'group by', 'order by', 
    'having', 'limit', 'offset', 'inner join', 'left join', 'right join', 'full join', 'cross join',
    'like', 'in', 'exists', 'between', 'and', 'or', 'not', 'null', 'is', 'case', 'when', 'then', 'else',
    'end', 'asc', 'desc', 'count', 'sum', 'avg', 'min', 'max', 'upper', 'lower', 'length',
    
    # Additional SQL Keywords
    'int', 'varchar', 'char', 'text', 'date', 'datetime', 'timestamp', 'boolean', 'float', 'double', 
    'decimal', 'blob', 'clob', 'time', 'grant', 'revoke', 'comment', 'rename', 'index', 'constraint', 
    'primary key', 'foreign key', 'check', 'unique', 'references', 'begin', 'commit', 'rollback', 
    'savepoint', 'natural join', 'using', 'declare', 'loop', 'while', 'for', 'return', 'raise', 
    'exception', 'cursor', 'fetch', 'open', 'close', 'row_number', 'rank', 'dense_rank', 'ntile', 
    'lag', 'lead', 'first_value', 'last_value', 'over', 'partition by', 'coalesce', 'nullif', 
    'substring', 'position', 'overlay', 'trim', 'rtrim', 'ltrim', 'replace', 'concat', 'decode', 
    'extract', 'interval', 'cast', 'convert', 'collate', 'session', 'current_user', 'current_date', 
    'current_timestamp', 'sysdate', 'user', 'group', 'role', 'timezone', 'auto_increment'
}

# Defining a set of SQL symbols and operators
sql_symbols = {
    '=', '!=', '<>', '<=', '>=', '>', '<', '(', ')', ',', ';', '--', '/*', '*/', "'", '"', "`",
    
    # Additional SQL Symbols and Operators
    '+', '-', '*', '/', '%', '^', '&', '|', '~', '&&', '||', '::', ':=', '->', '->>', '#>>', '#>', 
    '|/', '||/', '!!', ':', '@', '[', ']', '{}', '<<', '>>'
}

# Function to detect SQL syntax in a given text
def contains_sql_syntax(text):
    text = text.lower()
    keyword_found = any(re.search(r'\b' + keyword + r'\b', text) for keyword in sql_keywords)
    symbol_found = any(symbol in text for symbol in sql_symbols)
    pattern_found = bool(re.search(r'\b(select|insert|update|delete)\b\s+\*\s+\bfrom\b', text) or
                         re.search(r'\b\d+\s*=\s*\d+\b', text))
    return keyword_found or symbol_found or pattern_found

# Re-labeling data based on SQL syntax presence
main_df['Corrected_Label'] = main_df.apply(
    lambda row: 0 if row['Label'] == 0 and contains_sql_syntax(row['Query']) 
    else (2 if row['Label'] == 0 else 1),
    axis=1
)

print(main_df)
print(main_df['Corrected_Label'].value_counts())
print(main_df[main_df['Corrected_Label'] == 2].head(20))

# Plotting the distribution of corrected labels
f, ax = plt.subplots(1, 2, figsize=(12, 8))

main_df['Corrected_Label'].value_counts().plot.pie(
    explode=[0, 0.05, 0.1], autopct='%1.1f%%', ax=ax[0], shadow=True, wedgeprops={'alpha': 0.2}
)
ax[0].set_title('Corrected Label Distribution (Pie)')
ax[0].set_ylabel('')

sns.countplot(x='Corrected_Label', data=main_df, ax=ax[1], alpha=0.5)
ax[1].set_title('Corrected Label Distribution (Bar)')

plt.tight_layout()
plt.show()

# Adding advanced SQL injection data
additional_data = pd.read_csv("advanced_sql_injection_data.csv")

df_combined = pd.concat([main_df[['Query', 'Corrected_Label']], 
                         additional_data.rename(columns={'Label': 'Corrected_Label'})], 
                        ignore_index=True)
df_combined = df_combined.sample(frac=1, random_state=142).reset_index(drop=True)

print(df_combined['Corrected_Label'].value_counts())

# Downsampling class 2 to 10% of total data
class_2_data = df_combined[df_combined['Corrected_Label'] == 2]
downsampled_class_2 = class_2_data.sample(frac=0.1, random_state=42)

df_balanced = pd.concat([df_combined[df_combined['Corrected_Label'] != 2], downsampled_class_2], ignore_index=True)
df_balanced['Corrected_Label'] = df_balanced['Corrected_Label'].replace(2, 0)

#####################################################################################################################
# EDA: Plotting Corrected_Label distribution
#####################################################################################################################

##############################################################################
# plot 1 - Distribution of corrected Labels in the Balanced Dataset
##############################################################################

plt.figure(figsize=(10, 6))
sns.countplot(x='Corrected_Label', data=df_balanced, hue='Corrected_Label', palette='viridis', alpha=0.8, legend=False)
plt.title('Distribution of Corrected Labels in the Balanced Dataset', fontsize=16)
plt.xlabel('Corrected Label', fontsize=14)
plt.ylabel('Count', fontsize=14)

for p in plt.gca().patches:
    plt.gca().annotate(f'{int(p.get_height())}', 
                       (p.get_x() + p.get_width() / 2., p.get_height()), 
                       ha='center', va='center', fontsize=12, color='black', 
                       xytext=(0, 10), textcoords='offset points')

plt.tight_layout()
plt.show()


##############################################################################
# plot 2 - Counts of SQL Comment Symbols in Queries
##############################################################################

# Counting occurrences of specific SQL comment symbols in the queries
comment_symbols = ['#', '--', '//', '/*', '*/']
comment_counts = {symbol: 0 for symbol in comment_symbols}

# Iterating over each query in the dataset
for query in df_balanced['Query']:
    for symbol in comment_symbols:
        if symbol in query:
            comment_counts[symbol] += 1

# Converting the dictionary to a DataFrame for easier plotting
comment_counts_df = pd.DataFrame(list(comment_counts.items()), columns=['Comment Symbol', 'Count'])

# Plotting the counts of each comment symbol
plt.figure(figsize=(10, 6))
barplot = sns.barplot(x='Comment Symbol', y='Count', data=comment_counts_df, palette='viridis', alpha=0.8)

# Adding data labels on top of the bars
for p in barplot.patches:
    barplot.annotate(format(p.get_height(), '.0f'),
                     (p.get_x() + p.get_width() / 2., p.get_height()),
                     ha='center', va='center', fontsize=12, color='black', xytext=(0, 5),
                     textcoords='offset points')

# Adding titles and labels
plt.title('Counts of SQL Comment Symbols in Queries', fontsize=16)
plt.xlabel('Comment Symbol', fontsize=14)
plt.ylabel('Count', fontsize=14)
plt.tight_layout()
plt.show()

##############################################################################
# Plot 3 - Top 20 Most Frequent Words in SQL Injection Payloads
##############################################################################

# Setting the Seaborn theme for the plot with a white grid background and "talk" context for better readability
sns.set_theme(style="whitegrid", context="talk")

# Function to tokenize queries and count word frequency, excluding SQL keywords and integers
def tokenize_and_count_excluding_keywords_and_integers(query_list, sql_keywords):
    words = []
    for query in query_list:
        # Tokenize the query into words, converting all text to lowercase
        tokens = re.findall(r'\b\w+\b', query.lower())
        
        # Exclude SQL keywords and integers from the tokens
        filtered_tokens = [token for token in tokens if token not in sql_keywords and not token.isdigit()]
        
        # Add the filtered tokens to the list of words
        words.extend(filtered_tokens)
    
    # Return a Counter object with the frequency of each word
    return Counter(words)

# Tokenizing and counting words in SQL injection payloads (Label 1), excluding SQL keywords and integers
injection_queries = df_balanced[df_balanced['Corrected_Label'] == 1]['Query']
injection_word_count = tokenize_and_count_excluding_keywords_and_integers(injection_queries, sql_keywords)

# Getting the top 20 most frequent words from the word count
top_20_injection_words = injection_word_count.most_common(20)

# Creating a DataFrame for plotting the top 20 most frequent words
top_20_df = pd.DataFrame(top_20_injection_words, columns=['Word', 'Count'])

# Creating a horizontal bar chart for the top 20 most frequent words
plt.figure(figsize=(14, 8))  # Set the figure size
barplot = sns.barplot(x='Count', y='Word', data=top_20_df, palette='coolwarm', alpha=0.85)  # Use a coolwarm palette for the bars

# Adding titles and labels to the plot
plt.title('Top 20 Most Frequent Words in SQL Injection Payloads', fontsize=18, weight='bold')  # Title with bold text
plt.xlabel('Frequency Count', fontsize=14)  # X-axis label
plt.ylabel('Word', fontsize=14)  # Y-axis label

# Annotating the counts on the bars for better clarity
for index, value in enumerate(top_20_df['Count']):
    plt.text(value + 100, index, f'{value}', va='center', fontsize=12, color='black', weight='bold')  # Annotate on each bar
plt.tight_layout()
plt.show()


##############################################################################
# plot 4 - WordCloud for Keywords in Injection Queries vs Normal Queries
##############################################################################

# Generating word clouds for SQL injection payloads (Label 1)
injection_text = ' '.join(injection_queries)
wordcloud_injection = WordCloud(width=800, height=600, background_color='black').generate(injection_text)

# Generating word clouds for normal SQL queries (Label 0)
normal_queries = df_balanced[df_balanced['Corrected_Label'] == 0]['Query']
normal_text = ' '.join(normal_queries)
wordcloud_normal = WordCloud(width=800, height=600, background_color='white').generate(normal_text)

# Plotting the word clouds side by side for comparison
plt.figure(figsize=(16, 8))

# Word cloud for SQL injection payloads
plt.subplot(1, 2, 1)
plt.imshow(wordcloud_injection, interpolation='bilinear')
plt.title('Word Cloud - SQL Injection Queries', fontsize=18)
plt.axis('off')

# Word cloud for normal SQL queries
plt.subplot(1, 2, 2)
plt.imshow(wordcloud_normal, interpolation='bilinear')
plt.title('Word Cloud - Normal SQL Queries', fontsize=18)
plt.axis('off')

plt.tight_layout()
plt.show()


# Holding out 1% of the data for final validation using stratified sampling
df_balanced, df_validation = train_test_split(df_balanced, test_size=0.01, random_state=42, stratify=df_balanced['Corrected_Label'])

##################################################################################################################
# Data Tokenization
##################################################################################################################

# Tokenizer function to capture symbols, SQL syntax, comments, and special characters
def tokenizer(query):
    """
    Tokenizes the input SQL query to capture symbols, SQL syntax, comments, 
    and special characters using regular expressions.
    
    Parameters:
    - query (str): The SQL query to tokenize.
    
    Returns:
    - tokens (list): A list of tokens extracted from the query.
    """
    # Define a regex pattern to capture various SQL components
    token_pattern = re.compile(
        r'(--[^\n]*)|(/\*.*?\*/)|(\'.*?(?<!\\)\')|(".*?(?<!\\)")|(\b\w+\b)|([^\w\s])', 
        re.UNICODE | re.DOTALL
    )
    
    # Apply the pattern to the query and extract tokens
    tokens = token_pattern.findall(query)
    
    # Flatten the list of tokens by choosing the non-empty match group in each tuple
    tokens = [t[0] or t[1] or t[2] or t[3] or t[4] or t[5] for t in tokens]
    
    return tokens

# Applying the tokenizer to each query in the dataset
df_balanced['Query_tok'] = df_balanced['Query'].apply(tokenizer)

# Displaying the first 10 tokenized queries for inspection
print(df_balanced.head(10))

##############################################################################
# plot 5 - Distribution of Token Lengths in SQL Queries
##############################################################################

# Calculate the token length for each tokenized query
df_balanced['token_length'] = df_balanced['Query_tok'].apply(len)

plt.figure(figsize=(12, 8))
# Plotting the distribution of token lengths to analyze query complexity
sns.histplot(df_balanced['token_length'], bins=50, kde=True, color='#2a9d8f', edgecolor='black')

# Adding the title and axis labels with styling
plt.title('Distribution of Token Lengths in SQL Queries', fontsize=20, weight='bold', color='#264653')
plt.xlabel('Token Length', fontsize=16, weight='bold', color='#264653')
plt.ylabel('Frequency', fontsize=16, weight='bold', color='#264653')

# Customizing the plot background and removing the top and right spines for a cleaner look
plt.gca().set_facecolor('white')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['left'].set_visible(False)
plt.gca().spines['bottom'].set_visible(False)

# Calculating and plotting the mean token length as a vertical line
mean_length = df_balanced['token_length'].mean()
plt.axvline(mean_length, color='#e76f51', linestyle='--', linewidth=2)

# Annotating the mean token length on the plot
plt.text(mean_length + 1, plt.ylim()[1] * 0.9, f'Mean: {mean_length:.2f}', color='#e76f51', fontsize=14, weight='bold')

# Displaying the plot
plt.show()

# Filter out queries with a token length greater than 200 for further analysis
df_balanced = df_balanced[df_balanced['token_length'] <= 200].reset_index(drop=True)

##################################################################################################################
# Data embedding
##################################################################################################################

# Using the final balanced dataset
X = df_balanced['Query_tok']  # Tokenized queries
y = df_balanced['Corrected_Label']  # Corrected labels

# Reduce the dataset size to 10% for faster evaluation
X, _, y, _ = train_test_split(X, y, test_size=0.9, stratify=y, random_state=42)

# Split the reduced dataset into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=42)

def evaluate_embeddings(X_train, X_test, y_train, y_test, embeddings=['fasttext', 'char', 'bpe', 'bert']):
    """
    Evaluates different embedding methods by training LSTM models on them and 
    recording accuracy and training time.
    
    Parameters:
    - X_train, X_test: Training and test data (queries).
    - y_train, y_test: Corresponding labels.
    - embeddings (list): A list of embedding methods to evaluate ('fasttext', 'char', 'bpe', 'bert').
    
    Returns:
    - results (list): A list of dictionaries containing embedding method, accuracy, 
                      training time, and classification report.
    """
    results = []

    def build_lstm_model(input_shape):
        """
        Builds a simple LSTM model for sequence classification.
        
        Parameters:
        - input_shape (tuple): The shape of the input data.
        
        Returns:
        - model (Sequential): The compiled LSTM model.
        """
        model = Sequential([
            LSTM(64, input_shape=input_shape, return_sequences=False),
            Dropout(0.5),
            Dense(32, activation='relu'),
            Dense(3, activation='softmax')
        ])
        model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
        return model

    for embedding in embeddings:
        print(f"\nEvaluating with {embedding} embedding...")

        if embedding == 'fasttext':
            # Train FastText model on the training data
            fasttext_model = FastText(sentences=[query for query in X_train], vector_size=50, window=5, min_count=1, workers=4)

            # Embed and pad the queries for FastText
            def fasttext_embedding(queries, model, max_len=100):
                embedded_queries = [
                    [model.wv[word] for word in query if word in model.wv] for query in queries
                ]
                return pad_sequences(embedded_queries, maxlen=max_len, padding='post', dtype='float32')

            X_train_emb = fasttext_embedding(X_train, fasttext_model)
            X_test_emb = fasttext_embedding(X_test, fasttext_model)
            input_shape = (X_train_emb.shape[1], X_train_emb.shape[2])

        elif embedding == 'char':
            # Character-level tokenization and embedding
            char_tokenizer = Tokenizer(char_level=True)
            char_tokenizer.fit_on_texts(X_train)
            X_train_emb = pad_sequences(char_tokenizer.texts_to_sequences(X_train), maxlen=100, padding='post')
            X_test_emb = pad_sequences(char_tokenizer.texts_to_sequences(X_test), maxlen=100, padding='post')
            input_shape = (X_train_emb.shape[1], 1)

        elif embedding == 'bpe':
            # Byte Pair Encoding (BPE) tokenization
            bpe_tokenizer = ByteLevelBPETokenizer()
            bpe_tokenizer.train_from_iterator(X_train, vocab_size=30000, min_frequency=2)
            # Encoding and padding queries for BPE
            X_train_encoded = bpe_tokenizer.encode_batch([str(query) for query in X_train])
            X_test_encoded = bpe_tokenizer.encode_batch([str(query) for query in X_test])
            X_train_emb = pad_sequences([x.ids for x in X_train_encoded], maxlen=100, padding='post')
            X_test_emb = pad_sequences([x.ids for x in X_test_encoded], maxlen=100, padding='post')
            input_shape = (X_train_emb.shape[1], 1)

        elif embedding == 'bert':
            bert_tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
            bert_model = TFDistilBertModel.from_pretrained('distilbert-base-uncased')

            # Embed and pad the queries
            def bert_embedding(queries, tokenizer, model, max_len=100):
                inputs = tokenizer.batch_encode_plus(queries.tolist(), 
                                                     return_tensors='tf', 
                                                     padding=True, 
                                                     truncation=True, 
                                                     max_length=max_len)
                outputs = model(inputs['input_ids'], attention_mask=inputs['attention_mask'])

        # Build and train the LSTM model
        model = build_lstm_model(input_shape)
        start_time = time.time()
        model.fit(X_train_emb, y_train, validation_data=(X_test_emb, y_test), epochs=10, batch_size=16, verbose=1)
        training_time = time.time() - start_time

        # Evaluate the model's performance
        y_pred = model.predict(X_test_emb)
        y_pred_classes = np.argmax(y_pred, axis=1)
        accuracy = accuracy_score(y_test, y_pred_classes)
        report = classification_report(y_test, y_pred_classes, target_names=['Class 0', 'Class 1'])

        # Store the results for this embedding method
        results.append({
            'Embedding': embedding,
            'Accuracy': accuracy,
            'Training Time': training_time,
            'Classification Report': report
        })

    return results

# Running the evaluation for different embedding methods
results = evaluate_embeddings(X_train, X_test, y_train, y_test)

# Displaying the results for each embedding method
for result in results:
    print(f"\nEmbedding: {result['Embedding']}")
    print(f"Accuracy: {result['Accuracy']}")
    print(f"Training Time: {result['Training Time']} seconds")
    print(f"Classification Report:\n{result['Classification Report']}")

##############################################################################
# plot 6 - Performance of each Embedding method over Accuracy and Processing Time
##############################################################################

# Extracting data for plotting
embedding_methods = [result['Embedding'] for result in results]
accuracies = [result['Accuracy'] for result in results]
training_times = [result['Training Time'] for result in results]

# Creating a figure with two subplots side by side
fig, axes = plt.subplots(1, 2, figsize=(14, 6))

# Plotting Accuracy vs. Embedding Method
axes[0].plot(embedding_methods, accuracies, marker='o', linestyle='-', color='b', label='Accuracy')
axes[0].set_title('Accuracy by Embedding Method', fontsize=16)
axes[0].set_xlabel('Embedding Method', fontsize=14)
axes[0].set_ylabel('Accuracy', fontsize=14)
axes[0].set_ylim(0, 1)  # Assuming accuracy is between 0 and 1
axes[0].grid(True)
axes[0].legend()

# Plotting Training Time vs. Embedding Method
axes[1].plot(embedding_methods, training_times, marker='o', linestyle='-', color='r', label='Training Time')
axes[1].set_title('Training Time by Embedding Method', fontsize=16)
axes[1].set_xlabel('Embedding Method', fontsize=14)
axes[1].set_ylabel('Training Time (seconds)', fontsize=14)
axes[1].grid(True)
axes[1].legend()

plt.tight_layout()
plt.show()

##########################################################################################################
# Fasttext embedding is the best method for data embedding in terms of accuracy as well as in processing time.
# Training a FastText model on the entire dataset ()
# Use the entire dataset

X = df_balanced['Query_tok']  # Tokenized queries
y = df_balanced['Corrected_Label']  # Corrected labels

# Train FastText model on the entire dataset
fasttext_model = FastText(sentences=[query for query in X], vector_size=50, window=5, min_count=1, workers=4)

# Function to create FastText embeddings and pad them
def fasttext_embedding(queries, model, max_len=100):
    """
    Embeds queries using a trained FastText model and pads the sequences to a fixed length.
    
    Parameters:
    - queries (list): List of tokenized queries to embed.
    - model (FastText): Trained FastText model.
    - max_len (int): Maximum length to pad the sequences.
    
    Returns:
    - padded_queries (np.array): Numpy array of padded embeddings.
    """
    embedded_queries = [
        [model.wv[word] for word in query if word in model.wv] for query in queries
    ]
    padded_queries = pad_sequences(embedded_queries, maxlen=max_len, padding='post', dtype='float32')
    return np.array(padded_queries)

# Generate FastText embeddings for the entire dataset
X_emb = fasttext_embedding(X, fasttext_model, max_len=100)

# Display the shape of the embedded data
print(X_emb.shape)

##############################################################################
# plot 7 - Distribution of Embeddings for Class 0 and Class 1
##############################################################################

# Calculate the mean embedding for each query (across the token dimension)
mean_embeddings = np.mean(X_emb, axis=1)

# Combine the mean embeddings with the labels into a DataFrame
embedding_df = pd.DataFrame(mean_embeddings)
embedding_df['Label'] = y.values

# Reshape the DataFrame for Seaborn
melted_embedding_df = embedding_df.melt(id_vars=['Label'], var_name='Embedding Dimension', value_name='Embedding Value')

# Adjusting the figure size for more spacious layout
plt.figure(figsize=(18, 10))
# Plotting the distribution of embeddings for class 0 and class 1 using a boxen plot
sns.boxenplot(x='Label', y='Embedding Value', data=melted_embedding_df, palette='Set2')
plt.title('Distribution of Embeddings for Class 0 and Class 1', fontsize=20)
plt.xlabel('Class', fontsize=16)
plt.ylabel('Embedding Value', fontsize=16)
plt.grid(True, axis='y', linestyle='--', linewidth=0.7)
# Calculating summary statistics for annotation
summary_stats = melted_embedding_df.groupby('Label')['Embedding Value'].describe()
# Annotating summary statistics on the plot with improved placement
for i, label in enumerate(summary_stats.index):
    # Get median, Q1, and Q3
    median = summary_stats.loc[label, '50%']
    q1 = summary_stats.loc[label, '25%']
    q3 = summary_stats.loc[label, '75%']
    count = summary_stats.loc[label, 'count']

    # Annotate median slightly above the center
    plt.text(i, median + 0.5, f'Median: {median:.2f}', horizontalalignment='center', fontsize=12, color='black')
    
    # Annotate Q1 and Q3, offset from the median to avoid collision
    plt.text(i, q1 - 1, f'Q1: {q1:.2f}', horizontalalignment='center', fontsize=12, color='blue')
    plt.text(i, q3 + 1, f'Q3: {q3:.2f}', horizontalalignment='center', fontsize=12, color='blue')
    
    # Annotate count further down to avoid collision
    plt.text(i, q1 - 2, f'Count: {int(count)}', horizontalalignment='center', fontsize=12, color='green')

plt.show()

##################################################################################################################
# Data Encoding using Variational Autoencoder (VAE)
###################################################################################################################

# Clear previous Keras sessions to avoid potential conflicts
tf.keras.backend.clear_session() 

# Split the data into training and testing sets
# The input data X_emb has a shape of (32663, 100, 50)
X_train_emb, X_test_emb, y_train, y_test = train_test_split(X_emb, y, test_size=0.2, random_state=71)

# Define a function to normalize the data
def normalize_data(X_train, X_test):
    """
    Normalizes the training and testing data using Min-Max scaling.

    Args:
        X_train (np.ndarray): Training data.
        X_test (np.ndarray): Testing data.

    Returns:
        Tuple: Normalized training data, normalized testing data, and the scaler used.
    """
    scaler = MinMaxScaler()
    X_train_scaled = scaler.fit_transform(X_train.reshape(-1, X_train.shape[-1]))
    X_test_scaled = scaler.transform(X_test.reshape(-1, X_test.shape[-1]))
    X_train_scaled = X_train_scaled.reshape(X_train.shape)
    X_test_scaled = X_test_scaled.reshape(X_test.shape)
    return X_train_scaled, X_test_scaled, scaler

# Define the VAE model class
class VAE(tf.keras.Model):
    """
    Variational Autoencoder (VAE) model class.

    Args:
        input_shape (tuple): Shape of the input data (excluding batch dimension).
        latent_dim (int): Dimensionality of the latent space.
        encoder_layers (list): List of tuples defining the encoder layers.
    """
    def __init__(self, input_shape, latent_dim, encoder_layers):
        super(VAE, self).__init__()
        self.latent_dim = latent_dim
        self.input_shape = input_shape

        # Define the encoder network
        self.encoder_cnn = tf.keras.Sequential([
            layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same'),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten()
        ])
        self.encoder_lstm = layers.LSTM(units=256, return_sequences=False)
        self.z_mean = layers.Dense(latent_dim)
        self.z_log_var = layers.Dense(latent_dim)

        # Define the decoder network
        self.decoder_dense = layers.Dense(input_shape[0] * input_shape[1], activation='relu')
        self.decoder_output = layers.Reshape(input_shape)

    def encode(self, x):
        """
        Encodes the input data into the latent space.

        Args:
            x (tf.Tensor): Input tensor.

        Returns:
            Tuple: Mean and log variance of the latent distribution.
        """
        x_cnn_input = tf.expand_dims(x, -1)  # Add channel dimension for CNN
        x_cnn = self.encoder_cnn(x_cnn_input)
        
        x_lstm = self.encoder_lstm(x)  # LSTM expects 3D input
        
        x_combined = tf.concat([x_cnn, x_lstm], axis=-1)
        
        z_mean = self.z_mean(x_combined)
        z_log_var = self.z_log_var(x_combined)
        return z_mean, z_log_var

    def reparameterize(self, z_mean, z_log_var):
        """
        Reparameterizes the latent variables using the mean and log variance.

        Args:
            z_mean (tf.Tensor): Mean of the latent distribution.
            z_log_var (tf.Tensor): Log variance of the latent distribution.

        Returns:
            tf.Tensor: Reparameterized latent variables.
        """
        epsilon = tf.random.normal(shape=tf.shape(z_mean))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon

    def decode(self, z):
        """
        Decodes the latent variables back to the original space.

        Args:
            z (tf.Tensor): Latent variables.

        Returns:
            tf.Tensor: Reconstructed output.
        """
        x = self.decoder_dense(z)
        x = self.decoder_output(x)
        return x

    def call(self, inputs):
        """
        Forward pass through the VAE model.

        Args:
            inputs (tf.Tensor): Input data.

        Returns:
            Tuple: Reconstructed output, mean and log variance of the latent distribution.
        """
        z_mean, z_log_var = self.encode(inputs)
        z = self.reparameterize(z_mean, z_log_var)
        return self.decode(z), z_mean, z_log_var

# Define the VAE loss function
def vae_loss(inputs, outputs, z_mean, z_log_var):
    """
    Computes the VAE loss as a combination of reconstruction loss and KL divergence.

    Args:
        inputs (tf.Tensor): Original input data.
        outputs (tf.Tensor): Reconstructed data.
        z_mean (tf.Tensor): Mean of the latent distribution.
        z_log_var (tf.Tensor): Log variance of the latent distribution.

    Returns:
        tf.Tensor: Computed VAE loss.
    """
    reconstruction_loss = MeanSquaredError()(inputs, outputs)
    kl_loss = -0.5 * tf.reduce_mean(1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
    return reconstruction_loss + kl_loss

# Define the function for Bayesian hyperparameter tuning
def run_bayesian_tuning(X_train_subset, project_name):
    """
    Performs Bayesian optimization to find the best hyperparameters for the VAE model.

    Args:
        X_train_subset (np.ndarray): Subset of the training data.
        project_name (str): Name of the tuning project.

    Returns:
        dict: Best hyperparameters found during tuning.
    """
    class VAEHyperModel(HyperModel):
        def build(self, hp):
            input_shape = X_train_subset.shape[1:]
            latent_dim = hp.Int('latent_dim', min_value=64, max_value=512, step=64)
            num_layers = hp.Int('num_layers', min_value=2, max_value=6, step=1)

            encoder_layers = []
            for i in range(num_layers):
                units = hp.Int(f'neurons_{i}', min_value=128, max_value=1024, step=128)
                activation = hp.Choice(f'activation_{i}', values=['relu', 'tanh', 'swish'])
                use_regularizer = hp.Boolean(f'use_regularizer_{i}')
                regularizer = None
                if use_regularizer:
                    regularizer_type = hp.Choice(f'regularizer_type_{i}', values=['l1', 'l2', 'l1_l2'])
                    regularizer_strength = hp.Float(f'regularizer_strength_{i}', min_value=1e-5, max_value=1e-2, sampling='LOG')
                    if regularizer_type == 'l1':
                        regularizer = regularizers.l1(regularizer_strength)
                    elif regularizer_type == 'l2':
                        regularizer = regularizers.l2(regularizer_strength)
                    elif regularizer_type == 'l1_l2':
                        regularizer = regularizers.l1_l2(regularizer_strength)
                encoder_layers.append((units, activation, regularizer))
            
            vae = VAE(input_shape=input_shape, latent_dim=latent_dim, encoder_layers=encoder_layers)
            
            optimizer = Adam(learning_rate=hp.Choice('learning_rate', [1e-4, 5e-5, 1e-5]), clipnorm=1.0)
            
            vae.compile(optimizer=optimizer, loss=lambda x, y: vae_loss(x, y[0], y[1], y[2]))
            return vae

    tuner = BayesianOptimization(
        VAEHyperModel(),
        objective='val_loss',
        max_trials=7,
        executions_per_trial=1,
        directory='vae_bayesian_tuning',
        project_name=project_name
    )

    early_stopping = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
    lr_scheduler = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1, min_lr=1e-6)
    terminate_on_nan = TerminateOnNaN()

    tuner.search(X_train_subset, X_train_subset, epochs=50, validation_split=0.2, callbacks=[early_stopping, lr_scheduler, terminate_on_nan])

    return tuner.get_best_hyperparameters(num_trials=1)[0]

# Execute hyperparameter tuning on a fraction of the data
fractions = [0.15, 0.5]  # Evaluate on 15% and 50% of the training data

best_parameters = []
for fraction in fractions:
    print(f"Running trial with {fraction * 100}% of the data...")
    
    train_indices = random.sample(range(len(X_train_emb)), int(fraction * len(X_train_emb)))
    X_train_subset = X_train_emb[train_indices]
    
    X_train_scaled, X_test_scaled, scaler = normalize_data(X_train_subset, X_test_emb)

    best_hps = run_bayesian_tuning(X_train_scaled, f'vae_cnn_lstm_tuning_{int(fraction * 100)}')
    best_parameters.append(best_hps.values)

# Output the best hyperparameters found
print("Best Hyperparameters from all trials:", best_parameters)

# Normalize and reshape the entire training and testing data
X_train_scaled, X_test_scaled, scaler = normalize_data(X_train_emb, X_test_emb)

# Retrieve the best hyperparameters from the tuning process
best_hps = best_parameters[0]  # Select the best set of hyperparameters

latent_dim = best_hps['latent_dim']
num_layers = best_hps['num_layers']

encoder_layers = []
for i in range(num_layers):
    units = best_hps[f'neurons_{i}']
    activation = best_hps[f'activation_{i}']
    use_regularizer = best_hps[f'use_regularizer_{i}']
    regularizer = None
    if use_regularizer:
        regularizer_type = best_hps[f'regularizer_type_{i}']
        regularizer_strength = best_hps[f'regularizer_strength_{i}']
        if regularizer_type == 'l1':
            regularizer = regularizers.l1(regularizer_strength)
        elif regularizer_type == 'l2':
            regularizer = regularizers.l2(regularizer_strength)
        elif regularizer_type == 'l1_l2':
            regularizer = regularizers.l1_l2(regularizer_strength)
    encoder_layers.append((units, activation, regularizer))

# Define the VAE model using the best hyperparameters
vae = VAE(input_shape=X_train_scaled.shape[1:], latent_dim=latent_dim, encoder_layers=encoder_layers)

# Compile the VAE model with the selected optimizer and loss function
optimizer = Adam(learning_rate=best_hps['learning_rate'], clipnorm=1.0)
vae.compile(optimizer=optimizer, loss=lambda x, y: vae_loss(x, y[0], y[1], y[2]))

# Train the VAE model
early_stopping = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
lr_scheduler = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1, min_lr=1e-6)
terminate_on_nan = TerminateOnNaN()

history = vae.fit(X_train_scaled, X_train_scaled, 
                  epochs=50, 
                  validation_split=0.2, 
                  callbacks=[early_stopping, lr_scheduler, terminate_on_nan])

# Extract and plot the training and validation loss
loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(10, 6))
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.title('Training and Validation Loss over Epochs')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.grid(True)
plt.show()


############################################################################################################
# # Evaluation
############################################################################################################

# Get the reconstructed data
reconstructed_data, z_mean, z_log_var = vae.predict(X_test_scaled)

# Calculate Reconstruction Loss (Mean Squared Error)
reconstruction_loss = np.mean(np.square(X_test_scaled - reconstructed_data), axis=(1, 2))
average_reconstruction_loss = np.mean(reconstruction_loss)

# Mean Squared Error (MSE)
mse = mean_squared_error(X_test_scaled.reshape(-1), reconstructed_data.reshape(-1))

# Mean Absolute Error (MAE)
mae = mean_absolute_error(X_test_scaled.reshape(-1), reconstructed_data.reshape(-1))

# Calculate KL Divergence
kl_loss = -0.5 * np.mean(1 + z_log_var - np.square(z_mean) - np.exp(z_log_var))

# Print the Evaluation Metrics
print(f"Average Reconstruction Loss (MSE): {average_reconstruction_loss}")
print(f"Mean Squared Error (MSE): {mse}")
print(f"Mean Absolute Error (MAE): {mae}")
print(f"KL Divergence: {kl_loss}")

# Select a few samples from the test set
num_samples = 5
samples = X_test_scaled[:num_samples]

# Generate the reconstructed outputs
reconstructed, _, _ = vae.predict(samples)

import matplotlib.pyplot as plt
import numpy as np

# Define the number of samples to visualize
num_samples = 5

# Plotting the original and reconstructed data
fig, axes = plt.subplots(num_samples, 2, figsize=(8, 12))

for i in range(num_samples):
    # Original data
    axes[i, 0].imshow(samples[i].reshape(100, 50), cmap='viridis')
    axes[i, 0].set_title('Original', fontsize=14)
    axes[i, 0].axis('off')
    
    # Reconstructed data
    axes[i, 1].imshow(reconstructed[i].reshape(100, 50), cmap='viridis')
    axes[i, 1].set_title('Reconstructed', fontsize=14)
    axes[i, 1].axis('off')

plt.tight_layout()
plt.show()

##################################################################################################################
# Saving Model & Data
###################################################################################################################

# Concatenate the scaled data
X_combined_scaled = np.concatenate((X_train_scaled, X_test_scaled), axis=0)
y_combined = np.concatenate((y_train, y_test), axis=0)  


# Apply the VAE model to the scaled data
z_mean, _ = vae.encode(X_combined_scaled)

# Save the encoded data and corresponding labels separately
encoded_file_path = "encoded_combined_data.h5"

with h5py.File(encoded_file_path, "w") as h5f:
    h5f.create_dataset("encoded_data", data=z_mean)
    h5f.create_dataset("labels", data=y_combined)

print(f"Encoded data has been saved successfully to {encoded_file_path}.")

#################################
# Saving the Model
#################################

import tensorflow as tf
from tensorflow.keras import layers, regularizers
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import MeanSquaredError
import matplotlib.pyplot as plt

# Custom VAE class based on the provided best hyperparameters
@tf.keras.utils.register_keras_serializable()
class VAE(tf.keras.Model):
    def __init__(self, input_dim, latent_dim):
        super(VAE, self).__init__()
        self.latent_dim = latent_dim

        # Encoder: CNN + LSTM + Fully connected layers
        self.encoder_cnn = tf.keras.Sequential([
            layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same'),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten()
        ])

        self.encoder_lstm = layers.LSTM(units=256, return_sequences=False)

        # Fully connected layers as per the best hyperparameters
        self.dense_0 = layers.Dense(384, activation='relu', kernel_regularizer=regularizers.l1_l2(0.0025565480832756195))
        self.dense_1 = layers.Dense(768, activation='swish')
        self.dense_2 = layers.Dense(128, activation='relu', kernel_regularizer=regularizers.l1(1e-05))
        self.dense_3 = layers.Dense(640, activation='relu', kernel_regularizer=regularizers.l1_l2(5.055308034357991e-05))
        self.dense_4 = layers.Dense(512, activation='relu', kernel_regularizer=regularizers.l2(1.9672898529890975e-05))

        self.z_mean = layers.Dense(latent_dim)
        self.z_log_var = layers.Dense(latent_dim)

        # Decoder
        self.decoder_dense = layers.Dense(input_dim[0] * input_dim[1], activation='relu')
        self.decoder_output = layers.Reshape(input_dim)

    def encode(self, x):
        # CNN encoding part
        x_cnn_input = tf.expand_dims(x, -1)  # Add channel dimension for CNN
        x_cnn = self.encoder_cnn(x_cnn_input)
        
        # LSTM encoding part
        x_lstm = self.encoder_lstm(x)  # LSTM expects 3D input
        
        # Combine CNN and LSTM outputs
        x_combined = tf.concat([x_cnn, x_lstm], axis=-1)
        z_mean = self.z_mean(x_combined)
        z_log_var = self.z_log_var(x_combined)
        return z_mean, z_log_var

    def reparameterize(self, z_mean, z_log_var):
        epsilon = tf.random.normal(shape=tf.shape(z_mean))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon

    def decode(self, z):
        x = self.decoder_dense(z)
        x = self.decoder_output(x)
        return x

    def call(self, inputs):
        z_mean, z_log_var = self.encode(inputs)
        z = self.reparameterize(z_mean, z_log_var)
        return self.decode(z), z_mean, z_log_var

# VAE Loss Function
def vae_loss(inputs, outputs, z_mean, z_log_var):
    reconstruction_loss = MeanSquaredError()(inputs, outputs)
    kl_loss = -0.5 * tf.reduce_mean(1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
    return reconstruction_loss + kl_loss

# Instantiate and compile the VAE model with best hyperparameters
input_dim = X_train_scaled.shape[1:]  # Replace input_shape with input_dim
latent_dim = 448

vae_model = VAE(input_dim=input_dim, latent_dim=latent_dim)

# Compiling the VAE Model
optimizer = Adam(learning_rate=0.0001, clipnorm=1.0)
vae_model.compile(optimizer=optimizer, loss=lambda x, y: vae_loss(x, y[0], y[1], y[2]))

# Training the VAE Model
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
lr_scheduler = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1, min_lr=1e-6)
terminate_on_nan = tf.keras.callbacks.TerminateOnNaN()

history = vae_model.fit(X_train_scaled, X_train_scaled, 
                        epochs=50, 
                        validation_split=0.2, 
                        callbacks=[early_stopping, lr_scheduler, terminate_on_nan])

# Save the model in the Keras format
vae_model.save('vae_saved_model.keras')
print("Model saved successfully in .keras format")


############################################################################################################
# Encoding Validation data
############################################################################################################


# Tokenizing the df_validation dataset
df_validation['Query_tok'] = df_validation['Query'].apply(tokenizer)

# Generate FastText embeddings for the df_validation dataset
df_validation_emb = fasttext_embedding(df_validation['Query_tok'], fasttext_model, max_len=100)

# Normalize the df_validation_emb using the same scaler used for X_train_emb
df_validation_scaled, _, _ = normalize_data(df_validation_emb, df_validation_emb)  # Using df_validation_emb in both places since we're only normalizing one set

# Encode the df_validation_scaled dataset using the trained VAE model
z_mean_validation, z_log_var_validation = vae.encode(df_validation_scaled)

y_val = df_validation['Corrected_Label']

# Save the encoded validatoindata and corresponding labels separately
encoded_file_path = "encoded_validation_data.h5"

with h5py.File(encoded_file_path, "w") as h5f:
    h5f.create_dataset("encoded_data", data=z_mean_validation)
    h5f.create_dataset("labels", data=y_val)



#############################################################################################################
# Version details
############################################################################################################
import sys
import platform
import pandas as pd
import numpy as np
import matplotlib
import seaborn as sns
import gensim
from gensim.models import FastText, KeyedVectors
from sklearn import __version__ as sklearn_version
import tensorflow as tf
import Levenshtein
import nltk
import requests
import h5py
import joblib
import json
import pickle
import gzip
import keras_tuner
from tokenizers import ByteLevelBPETokenizer

def print_package_versions():
    print("Python version:", sys.version)
    print("Platform:", platform.platform())
    
    print("\n--- Data Manipulation and Analysis ---")
    print("Pandas version:", pd.__version__)
    print("NumPy version:", np.__version__)
    
    print("\n--- Data Visualization ---")
    print("Matplotlib version:", matplotlib.__version__)
    print("Seaborn version:", sns.__version__)
    
    print("\n--- Natural Language Processing and Embeddings ---")
    print("Gensim version:", gensim.__version__)
    print("FastText version:", FastText.__module__.split('.')[1])  # FastText is a part of Gensim
    print("KeyedVectors version:", KeyedVectors.__module__.split('.')[1])
    
    print("\n--- Machine Learning and Model Evaluation ---")
    print("Scikit-learn version:", sklearn_version)
    
    print("\n--- Deep Learning ---")
    print("TensorFlow version:", tf.__version__)
    
    print("\n--- Transformers and Tokenizers ---")
    print("ByteLevelBPETokenizer version:", ByteLevelBPETokenizer.__module__.split('.')[1])
    
    print("\n--- Additional Tools ---")
    print("Requests version:", requests.__version__)
    print("H5py version:", h5py.__version__)
    print("Joblib version:", joblib.__version__)
    print("Pickle version: Built-in Python module")
    print("JSON version: Built-in Python module")
    print("Gzip version: Built-in Python module")
    print("Levenshtein version:", Levenshtein.__version__)
    print("NLTK version:", nltk.__version__)
    print("Random version: Built-in Python module")
    print("Time version: Built-in Python module")
    print("Collections version: Built-in Python module")
    
    print("\n--- Hyperparameter Tuning ---")
    print("Keras Tuner version:", keras_tuner.__version__)

print_package_versions()


with open("package_versions.txt", "w") as file:
    file.write("Python version: " + sys.version + "\n")
    file.write("Platform: " + platform.platform() + "\n")
    
    file.write("\n--- Data Manipulation and Analysis ---\n")
    file.write("Pandas version: " + pd.__version__ + "\n")
    file.write("NumPy version: " + np.__version__ + "\n")
    
    file.write("\n--- Data Visualization ---\n")
    file.write("Matplotlib version: " + matplotlib.__version__ + "\n")
    file.write("Seaborn version: " + sns.__version__ + "\n")
    
    file.write("\n--- Natural Language Processing and Embeddings ---\n")
    file.write("Gensim version: " + gensim.__version__ + "\n")
    file.write("FastText version: " + FastText.__module__.split('.')[1] + "\n")
    file.write("KeyedVectors version: " + KeyedVectors.__module__.split('.')[1] + "\n")
    
    file.write("\n--- Machine Learning and Model Evaluation ---\n")
    file.write("Scikit-learn version: " + sklearn_version + "\n")
    
    file.write("\n--- Deep Learning ---\n")
    file.write("TensorFlow version: " + tf.__version__ + "\n")
    
    file.write("\n--- Transformers and Tokenizers ---\n")
    file.write("ByteLevelBPETokenizer version: " + ByteLevelBPETokenizer.__module__.split('.')[1] + "\n")
    
    file.write("\n--- Additional Tools ---\n")
    file.write("Requests version: " + requests.__version__ + "\n")
    file.write("H5py version: " + h5py.__version__ + "\n")
    file.write("Joblib version: " + joblib.__version__ + "\n")
    file.write("Pickle version: Built-in Python module\n")
    file.write("JSON version: Built-in Python module\n")
    file.write("Gzip version: Built-in Python module\n")
    file.write("Levenshtein version: " + Levenshtein.__version__ + "\n")
    file.write("NLTK version: " + nltk.__version__ + "\n")
    file.write("Random version: Built-in Python module\n")
    file.write("Time version: Built-in Python module\n")
    file.write("Collections version: Built-in Python module\n")
    
    file.write("\n--- Hyperparameter Tuning ---\n")
    file.write("Keras Tuner version: " + keras_tuner.__version__ + "\n")
