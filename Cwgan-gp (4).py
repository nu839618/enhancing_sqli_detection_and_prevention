# Core Libraries
import numpy as np
import h5py
import json
import matplotlib.pyplot as plt

# TensorFlow and Keras
import tensorflow as tf
from tensorflow.keras import layers, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau

# Machine Learning Libraries
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.metrics import mean_squared_error, r2_score, explained_variance_score, accuracy_score, silhouette_score, roc_auc_score
from sklearn.decomposition import PCA

# Hyperparameter Tuning
import keras_tuner as kt
import optuna

# Text Metrics
from Levenshtein import distance as levenshtein_distance
from sklearn.metrics.pairwise import cosine_similarity
from nltk.translate.bleu_score import sentence_bleu


# Load and prepare data
hdf5_file_path = "encoded_combined_data.h5"
with h5py.File(hdf5_file_path, "r") as h5f:
    encoded_data = np.array(h5f["encoded_data"])
    labels = np.array(h5f["labels"])

# Using only 20% of the data for hyperparameter tuning.
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
for train_index, test_index in sss.split(encoded_data, labels):
    X_train_20, X_temp = encoded_data[train_index], encoded_data[test_index]
    y_train_20, y_temp = labels[train_index], labels[test_index]

# Splitting the 30% data into training and validation sets (75% train, 25% validation)
X_train_20, X_val_20, y_train_20, y_val_20 = train_test_split(X_train_20, y_train_20, test_size=0.25, stratify=y_train_20, random_state=42)

# Defining input and output dimensions and the number of classes based on the data
input_dim = X_train_20.shape[1]
output_dim = X_train_20.shape[1]
num_classes = np.max(labels) + 1


################################################################################################################
# 1. Generator 
################################################################################################################

# Model for Bayesian optimization with labels
def build_generator_model_with_labels(hp, input_dim, num_classes):
    """
    Builds a generator model for the Conditional Wasserstein GAN with Gradient Penalty (CWGAN-GP) 
    tailored for SQL injection data augmentation.

    This generator model is designed to generate synthetic SQL injection sequences conditioned on 
    specific labels (e.g., types of SQL injection attacks). The model takes as input a noise vector 
    and a one-hot encoded label, concatenates them, and processes the result through a series of 
    dense layers to produce synthetic SQL sequences.

    The architecture of the model, including the number of layers, units per layer, activation functions, 
    and dropout rates, is determined by hyperparameters provided by Keras Tuner.

    Args:
        hp (kerastuner.HyperParameters): Hyperparameters for tuning the model.
        input_dim (int): Dimensionality of the input noise vector.
        num_classes (int): Number of classes for the label input (e.g., different types of SQL injections).

    Returns:
        Model: A compiled Keras model that generates synthetic SQL sequences conditioned on the label input.
    """
    
    # Define the input layer for the noise vector (latent space)
    noise_input = layers.Input(shape=(input_dim,))
    
    # Define the input layer for the one-hot encoded label
    label_input = layers.Input(shape=(num_classes,))
    
    # Concatenate the noise vector and label input to condition the generation process
    x = layers.Concatenate()([noise_input, label_input])
    
    # Add a series of dense layers as specified by hyperparameters
    for i in range(hp.Int('num_layers', 1, 6)):
        x = layers.Dense(units=hp.Int(f'units_{i}', min_value=32, max_value=1024, step=64),
                         activation=hp.Choice('activation', ['relu', 'tanh']),
                         kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        x = layers.Dropout(rate=hp.Float(f'dropout_{i}', min_value=0.0, max_value=0.6, step=0.02))(x)
    
    # Output layer that generates the synthetic SQL sequence (same dimensionality as input_dim)
    output = layers.Dense(input_dim, activation='linear')(x)
    
    # Compile the model with Adam optimizer and Mean Squared Error loss
    model = Model([noise_input, label_input], output)
    model.compile(optimizer=tf.keras.optimizers.Adam(hp.Float('learning_rate', 1e-8, 1e-2, sampling='log')),
                  loss='mse', 
                  metrics=[tf.keras.metrics.MeanSquaredError(name='reconstruction_loss')])
    
    return model


# Initialize the Bayesian tuner
tuner = kt.BayesianOptimization(
    lambda hp: build_generator_model_with_labels(hp, input_dim, num_classes),
    objective=kt.Objective('val_reconstruction_loss', direction='min'),
    max_trials=30,
    directory='my_dir/cwgangp/',
    project_name='bayesian_initial_tuning'
)

# Running the initial Bayesian tuning
tuner.search([X_train_20, tf.one_hot(y_train_20, num_classes)], X_train_20, epochs=10, validation_data=([X_val_20, tf.one_hot(y_val_20, num_classes)], X_val_20),
             callbacks=[tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3),
                        tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, min_lr=1e-6)])

# Getting the best parameters from Bayesian optimization
best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]

# Optuna objective function for fine-tuning using reconstruction loss
def generator_objective(trial):
    """
    Defines the objective function for fine-tuning the generator model using Optuna.

    The function builds and compiles a generator model based on the hyperparameters suggested
    by Optuna and the best hyperparameters from previous tuning (stored in `best_hps`).
    The model is then trained on a subset of the data, and the validation reconstruction loss
    is returned as the objective to be minimized.

    Args:
        trial (optuna.trial.Trial): The trial object to suggest hyperparameters for the model.

    Returns:
        float: The minimum validation reconstruction loss achieved during training.
    """
    # Define the input layers
    noise_input = layers.Input(shape=(input_dim,))
    label_input = layers.Input(shape=(num_classes,))
    
    # Concatenate noise and label inputs to condition the generation process
    x = layers.Concatenate()([noise_input, label_input])
    
    # Add dense and dropout layers based on Optuna's hyperparameter suggestions and best_hps
    for i in range(best_hps.get('num_layers')):
        x = layers.Dense(units=trial.suggest_int(f'units_{i}', best_hps.get(f'units_{i}') - 32, best_hps.get(f'units_{i}') + 32, step=16),
                         activation=trial.suggest_categorical('activation', ['relu', 'tanh']),
                         kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        x = layers.Dropout(rate=trial.suggest_float(f'dropout_{i}', 
                                                    max(0, best_hps.get(f'dropout_{i}') - 0.05), 
                                                    min(1, best_hps.get(f'dropout_{i}') + 0.05), 
                                                    step=0.01))(x)
    
    # Output layer generating the synthetic SQL sequence
    output = layers.Dense(output_dim, activation='linear')(x)
    
    # Compile the model
    model = Model([noise_input, label_input], output)
    model.compile(optimizer=tf.keras.optimizers.Adam(trial.suggest_float('learning_rate', best_hps.get('learning_rate') / 2, best_hps.get('learning_rate') * 2, log=True)),
                  loss='mse', 
                  metrics=[tf.keras.metrics.MeanSquaredError(name='reconstruction_loss')])

    # Train the model
    history = model.fit([X_train_20, tf.one_hot(y_train_20, num_classes)], X_train_20, 
                        epochs=10, 
                        validation_data=([X_val_20, tf.one_hot(y_val_20, num_classes)], X_val_20), 
                        verbose=0,
                        callbacks=[tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3),
                                   tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, min_lr=1e-6)])
    
    # Return the minimum validation loss as the objective to minimize
    val_loss = min(history.history['val_reconstruction_loss'])
    return val_loss

# Optuna fine-tuning
study = optuna.create_study(direction='minimize')
study.optimize(generator_objective, n_trials=20)

# Best parameters from Optuna
best_params = study.best_params
print(f"Best hyperparameters: {best_params}")


# Training the final model on complete data
X_train, X_val, y_train, y_val = train_test_split(encoded_data, labels, test_size=0.25, stratify=labels, random_state=42)



# Building the final generator model with the best hyperparameters
final_generator = build_generator_model_with_labels(best_hps, input_dim, num_classes)

final_generator.compile(optimizer=tf.keras.optimizers.Adam(best_params['learning_rate']),
                        loss='mse', 
                        metrics=[tf.keras.metrics.MeanSquaredError(name='reconstruction_loss')])

# Early stopping and learning rate scheduler
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)
lr_scheduler = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, min_lr=1e-6)

# Train the final model with callbacks
history = final_generator.fit([X_train, tf.one_hot(y_train, num_classes)], X_train, epochs=100, validation_data=([X_val, tf.one_hot(y_val, num_classes)], X_val), verbose=2,
                               callbacks=[early_stopping, lr_scheduler])

# Plotting the reconstruciton losses of generator model
plt.figure(figsize=(10, 5))
plt.plot(history.history['reconstruction_loss'], label='Reconstruction Loss')
plt.plot(history.history['val_reconstruction_loss'], label='Val Reconstruction Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.title('Training and Validation Loss')
plt.show()

# Saving the generator model
final_generator.save("best_generator.h5")


#####################################################################################################################
## 2. Discriminator (Critic)
#####################################################################################################################

# The input dimensions and number of classes for the critic model are defined.
input_dim = 448  # The dimensionality of the SQL injection data (features)
num_classes = 2  # The number of classes for labels (e.g., benign or malicious)

# The model architecture for the Critic (Discriminator) in CWGAN-GP is specified.
def build_critic_model(input_dim, num_classes):
    """
    Constructs a Critic (Discriminator) model for the CWGAN-GP architecture.

    This model combines the data input (SQL injection features) with the corresponding
    one-hot encoded labels. The combined inputs pass through several
    fully connected layers with ReLU activations and dropout for regularization.

    Args:
        input_dim (int): Dimensionality of the input data (e.g., number of features in the SQL injection data).
        num_classes (int): Number of classes for the labels (e.g., 2 for binary classification).

    Returns:
        tf.keras.Model: A Keras model representing the critic.
    """
    # Data input (SQL injection features) and label input (one-hot encoded labels) are defined.
    data_input = layers.Input(shape=(input_dim,))
    label_input = layers.Input(shape=(num_classes,))
    
    # Data and label inputs are concatenated to condition the discrimination process.
    x = layers.Concatenate()([data_input, label_input])
    
    # Dense layers with ReLU activation and dropout for regularization are included.
    x = layers.Dense(512, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    x = layers.Dense(256, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    x = layers.Dense(128, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    
    # An output layer for the critic, predicting the realness of the input (linear activation), is included.
    output = layers.Dense(1, activation='linear')(x)
    
    # Model is returned with the defined inputs and output.
    return Model(inputs=[data_input, label_input], outputs=output)

# The critic model for the CWGAN-GP is instantiated using the specified input dimensions and number of classes.
critic_model = build_critic_model(input_dim, num_classes)

# The model is compiled with the Adam optimizer and mean squared error (MSE) loss function.
critic_model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0002),
                     loss='mse')

# The training and validation labels are prepared as one-hot encoded vectors for conditioning.
y_train_one_hot = tf.one_hot(y_train, num_classes)
y_val_one_hot = tf.one_hot(y_val, num_classes)

# Early stopping and learning rate reduction callbacks are defined to prevent overfitting and adjust the learning rate during training.
early_stopping = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, min_lr=1e-6)

# The critic model is trained using the training data, with validation data provided for monitoring.
history = critic_model.fit([X_train, y_train_one_hot], y_train,
                           epochs=30,
                           batch_size=64,
                           validation_data=([X_val, y_val_one_hot], y_val),
                           callbacks=[early_stopping, reduce_lr])

# The final architecture and parameters of the trained critic model are summarized.
critic_model.summary()

# Plotting the loss
plt.plot(history.history['loss'], label='Training Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.title('Training and Validation Loss')
plt.show()

# Saving the Discriminator model
critic_model.save("best_discriminator.h5")

####################################################################################################################
# CWGAN-GP 
####################################################################################################################

# Clear any existing TensorFlow sessions to avoid potential conflicts
tf.keras.backend.clear_session()

# Define the input dimensions, number of classes, and latent dimension for the generator
input_dim = 448  
num_classes = 2 
latent_dim = 100  

# Generator model with labels

def build_generator_with_labels(input_dim, latent_dim, num_classes):
    # Define the input layers for noise and labels
    noise_input = layers.Input(shape=(latent_dim,))
    label_input = layers.Input(shape=(num_classes,))

    # Concatenate the noise and label inputs
    x = layers.Concatenate()([noise_input, label_input])

    # Add dense layers with ReLU activation
    x = layers.Dense(512, activation='relu')(x)
    x = layers.Dense(256, activation='relu')(x)
    
    # Final dense layer with linear activation to match the input dimension
    x = layers.Dense(input_dim, activation='linear')(x)

    # Define and return the generator model
    model = Model([noise_input, label_input], x)
    return model

# Critic (Discriminator) model with labels
def build_critic_with_labels(input_dim, num_classes):
    # Define the input layers for data and labels
    data_input = layers.Input(shape=(input_dim,))
    label_input = layers.Input(shape=(num_classes,))

    # Concatenate the data and label inputs
    x = layers.Concatenate()([data_input, label_input])
    
    # Add dense layers with ReLU activation
    x = layers.Dense(512, activation='relu')(x)
    x = layers.Dense(256, activation='relu')(x)
    
    # Final dense layer with linear activation for the output
    x = layers.Dense(1, activation='linear')(x)

    # Define and return the critic model
    model = Model([data_input, label_input], x)
    return model

# Instantiate the Generator and Critic models
generator = build_generator_with_labels(input_dim, latent_dim, num_classes)
critic = build_critic_with_labels(input_dim, num_classes)

# Define the Wasserstein loss function for CWGAN
def wasserstein_loss(y_true, y_pred):
    return tf.reduce_mean(y_true * y_pred)

# Function to compute the gradient penalty for the critic
def gradient_penalty(critic, real_data, fake_data, real_labels):
    """ Computes the gradient penalty for WGAN-GP """
    batch_size = tf.shape(real_data)[0]
    
    # Interpolate between real and fake data
    epsilon = tf.random.uniform([batch_size, 1], 0.0, 1.0)
    interpolated_data = epsilon * real_data + (1 - epsilon) * fake_data
    
    # Calculate gradients for the interpolated data
    with tf.GradientTape() as tape:
        tape.watch(interpolated_data)
        interpolated_output = critic([interpolated_data, real_labels], training=True)

    gradients = tape.gradient(interpolated_output, [interpolated_data])[0]
    gradients_norm = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[1]))

    # Calculate the penalty term
    penalty = tf.reduce_mean(tf.square(gradients_norm - 1.0))
    return penalty


# Optimizers for the Generator and Critic models
# Using the learning rate obtained from the best hyperparameters found during tuning
generator_optimizer = Adam(learning_rate=best_params['learning_rate'], beta_1=0.5, beta_2=0.9)
critic_optimizer = Adam(learning_rate=best_params['learning_rate'], beta_1=0.5, beta_2=0.9)

# Training loop configuration for CWGAN-GP
epochs = 6000  # Number of total epochs for training
batch_size = 128  # Number of samples in each batch
n_critic = 2  # Number of critic updates for each generator update
lambda_gp = 15  # Coefficient for the gradient penalty term

# Convert the training labels to one-hot encoded vectors
y_train_one_hot = tf.one_hot(y_train, num_classes)
y_val_one_hot = tf.one_hot(y_val, num_classes)

# Initialize lists to store the generator and critic losses during training for plotting
g_losses = []
d_losses = []

for epoch in range(epochs):
    for _ in range(n_critic):
        # Sample a random batch of data from the training set
        idx = np.random.randint(0, X_train.shape[0], batch_size)
        real_data = X_train[idx]  # Get the real data samples
        real_labels = tf.gather(y_train_one_hot, idx)  # Get the corresponding one-hot encoded labels

        # Generate fake data using the generator
        noise = tf.random.normal([batch_size, latent_dim])  # Generate random noise vectors
        fake_labels = tf.one_hot(np.random.randint(0, num_classes, batch_size), num_classes)  # Generate random one-hot labels
        fake_data = generator([noise, fake_labels])  # Generate fake data based on noise and fake labels

        # Train the Critic (Discriminator)
        with tf.GradientTape() as tape_d:
            real_output = critic([real_data, real_labels])  # Get the critic's prediction on real data
            fake_output = critic([fake_data, fake_labels])  # Get the critic's prediction on fake data
            d_loss_real = wasserstein_loss(-tf.ones_like(real_output), real_output)  # Calculate the loss for real data
            d_loss_fake = wasserstein_loss(tf.ones_like(fake_output), fake_output)  # Calculate the loss for fake data
            gp = gradient_penalty(critic, real_data, fake_data, real_labels)  # Calculate the gradient penalty
            d_loss = d_loss_real + d_loss_fake + lambda_gp * gp  # Total critic loss including gradient penalty
        
        # Compute the gradients for the critic and apply them
        grads_d = tape_d.gradient(d_loss, critic.trainable_variables)
        critic_optimizer.apply_gradients(zip(grads_d, critic.trainable_variables))
    
    # Train the Generator
    noise = tf.random.normal([batch_size, latent_dim])  # Generate a new batch of random noise vectors
    fake_labels = tf.one_hot(np.random.randint(0, num_classes, batch_size), num_classes)  # Generate random one-hot labels for generator training
    
    with tf.GradientTape() as tape_g:
        generated_data = generator([noise, fake_labels])  # Generate fake data with the generator
        fake_output = critic([generated_data, fake_labels])  # Get the critic's prediction on the generated data
        g_loss = wasserstein_loss(-tf.ones_like(fake_output), fake_output)  # Calculate the generator's loss

    # Compute the gradients for the generator and apply them
    grads_g = tape_g.gradient(g_loss, generator.trainable_variables)
    generator_optimizer.apply_gradients(zip(grads_g, generator.trainable_variables))

    # Store the current epoch's losses for later analysis
    g_losses.append(g_loss.numpy())
    d_losses.append(d_loss.numpy())

    # Output the losses for this epoch
    print(f'Epoch {epoch+1}/{epochs} - Generator Loss: {g_loss.numpy():.4f}, Critic Loss: {d_loss.numpy():.4f}')

# Smoothing the loss curvers
def smooth_curve(points, factor=0.9):
    """
    Smooths a list of points using exponential moving average.

    Parameters:
    - points: List of loss values.
    - factor: Smoothing factor. Higher values give more weight to previous points.

    Returns:
    - smoothed_points: List of smoothed loss values.
    """
    smoothed_points = []
    for point in points:
        if smoothed_points:
            previous = smoothed_points[-1]
            smoothed_points.append(previous * factor + point * (1 - factor))
        else:
            smoothed_points.append(point)
    return smoothed_points

# Apply smoothing to the generator and critic loss data
smoothed_g_losses = smooth_curve(g_losses)
smoothed_d_losses = smooth_curve(d_losses)

# Plot the smoothed losses
plt.figure(figsize=(10, 5))
plt.plot(smoothed_g_losses, label='Generator Loss')
plt.plot(smoothed_d_losses, label='Critic Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.title('CWGAN Generator and Critic Losses with Gradient Penalty')
plt.show()

################################################################################################################
# Generating Synthetic Data & Model Evaluation
################################################################################################################

# Function to generate synthetic data using the trained generator model
def generate_synthetic_data(model, num_samples, input_dim, num_classes):
    """
    Generates synthetic data using the trained generator model.

    Parameters:
    - model: The trained generator model.
    - num_samples: The number of synthetic samples to generate.
    - input_dim: The dimensionality of the noise input.
    - num_classes: The number of classes for label generation.

    Returns:
    - synthetic_data: The generated synthetic data.
    """
    noise = tf.random.normal([num_samples, input_dim])
    fake_labels = tf.one_hot(np.random.randint(0, num_classes, num_samples), num_classes)
    synthetic_data = model.predict([noise, fake_labels])
    return synthetic_data

# Generate synthetic samples using the generator model
num_synthetic_samples = 10000
synthetic_data = generate_synthetic_data(generator, num_synthetic_samples, latent_dim, num_classes)

# Function to evaluate the synthetic data against real data
def evaluate_synthetic_data(synthetic_data, real_data):
    """
    Evaluates the generated synthetic data against the real data.

    Parameters:
    - synthetic_data: The generated synthetic data.
    - real_data: The real data to compare against.

    Prints:
    - Reconstruction metrics: MSE, R2 Score, Explained Variance Score, BLEU score, Cosine Similarity, Levenshtein Distance.
    - Distributional Comparison: Mean Difference, Variance Difference.
    - PCA Visualization of real vs. synthetic data.
    """
    assert synthetic_data.shape == real_data.shape, f"Shape mismatch: Synthetic {synthetic_data.shape}, Real {real_data.shape}"
    
    synthetic_data_flat = synthetic_data.reshape(synthetic_data.shape[0], -1)
    real_data_flat = real_data.reshape(real_data.shape[0], -1)
    
    # Calculate reconstruction metrics
    mse = mean_squared_error(real_data_flat, synthetic_data_flat)
    r2 = r2_score(real_data_flat, synthetic_data_flat)
    evs = explained_variance_score(real_data_flat, synthetic_data_flat)
    
    print(f'MSE: {mse:.4f}')
    print(f'R2 Score: {r2:.4f}')
    print(f'Explained Variance Score: {evs:.4f}')

    # Calculate BLEU score
    bleu_scores = []
    for i in range(real_data_flat.shape[0]):
        real_seq = [int(x) for x in real_data_flat[i]]
        synthetic_seq = [int(x) for x in synthetic_data_flat[i]]
        bleu_score = sentence_bleu([real_seq], synthetic_seq)
        bleu_scores.append(bleu_score)
    
    avg_bleu_score = np.mean(bleu_scores)
    print(f'Average BLEU Score: {avg_bleu_score:.4f}')
    
    # Calculate Cosine Similarity
    avg_cosine_similarity = np.mean([cosine_similarity([real_data_flat[i]], [synthetic_data_flat[i]])[0, 0] for i in range(real_data_flat.shape[0])])
    print(f'Average Cosine Similarity: {avg_cosine_similarity:.4f}')
    
    # Calculate Levenshtein Distance
    levenshtein_distances = []
    for i in range(real_data_flat.shape[0]):
        real_seq = real_data_flat[i].astype(int)
        synthetic_seq = synthetic_data_flat[i].astype(int)
        levenshtein_distances.append(levenshtein_distance(real_seq, synthetic_seq))
    
    avg_levenshtein_distance = np.mean(levenshtein_distances)
    print(f'Average Levenshtein Distance: {avg_levenshtein_distance:.4f}')
    
    # Distributional Comparison
    mean_diff = np.abs(np.mean(real_data_flat) - np.mean(synthetic_data_flat))
    var_diff = np.abs(np.var(real_data_flat) - np.var(synthetic_data_flat))
    
    print(f'Mean Difference: {mean_diff:.4f}')
    print(f'Variance Difference: {var_diff:.4f}')
    
    # PCA Visualization for synthetic and real data
    pca = PCA(n_components=2)
    real_data_pca = pca.fit_transform(real_data_flat)
    synthetic_data_pca = pca.transform(synthetic_data_flat)
    
    plt.figure(figsize=(10, 6))
    plt.scatter(real_data_pca[:, 0], real_data_pca[:, 1], color='blue', label='Real Data', alpha=0.5)
    plt.scatter(synthetic_data_pca[:, 0], synthetic_data_pca[:, 1], color='red', label='Synthetic Data', alpha=0.5)
    plt.title('PCA of Real vs Synthetic Data')
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')
    plt.legend()
    plt.show()

# Evaluate the synthetic data against real data
evaluate_synthetic_data(synthetic_data, X_train[:num_synthetic_samples])

# Save the best hyperparameters to a JSON file
best_params_file = 'best_hyperparameters.json'
with open(best_params_file, 'w') as f:
    json.dump(best_params, f)
print(f'Best hyperparameters saved to {best_params_file}')

# Save the synthetic data as an H5 file
output_file_path = "cwgangp_syn_data.h5"
with h5py.File(output_file_path, "w") as h5f:
    h5f.create_dataset("cwgangp_syn_data", data=synthetic_data)
print(f'Synthetic data saved to {output_file_path}')
